import React, { Component } from 'react';
import { YellowBox } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './src/store/configureStore';
import axios from 'axios';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const store = configureStore();

import RootNavigatorWithState from './src/RootNavigatorWithState';

export default class App extends Component{

    componentWillMount () {

        axios.defaults.baseURL = '';

    }

    render () {

        return (
            <Provider store={store}>
                <RootNavigatorWithState />
            </Provider>
        );

    }

}

