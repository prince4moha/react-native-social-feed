import React , { Component } from 'react';
import RootNavigator from './routes/index';
import { addNavigationHelpers } from 'react-navigation';
import { createReduxBoundAddListener } from "react-navigation-redux-helpers";
import { connect } from 'react-redux';

const addListener = createReduxBoundAddListener("root");

class RootNavigatorWithState extends Component {

    render () {

        return (

            <RootNavigator navigation={addNavigationHelpers({
                dispatch: this.props.dispatch,
                state: this.props.nav,
                addListener,
            })} />

        );

    }

}

const mapStateToProps = (state) => ({
    nav: state.nav
});

export default connect(mapStateToProps)(RootNavigatorWithState);
