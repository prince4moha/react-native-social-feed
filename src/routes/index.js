import React from 'react';

import { StackNavigator , TabNavigator , DrawerNavigator , SwitchNavigator } from 'react-navigation';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';
import SigninScreen from '../screens/SigninScreen';
import SignupScreen from '../screens/SignupScreen';
import FeedScreen from '../screens/FeedScreen';
import PostScreen from '../screens/PostScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SavedPostsScreen from '../screens/SavedPostsScreen';
import SettingsScreen from '../screens/SettingsScreen';
import CommentsScreen from '../screens/CommentsScreen';
import CreatePostScreen from '../screens/CreatePostScreen';
import EditPostScreen from '../screens/EditPostScreen';
import EditProfileScreen from '../screens/EditProfileScreen';

import TestScreen from '../screens/TestScreen';

import DrawerContent from '../screens/components/DrawerContent';
import theme from '../utils/theme';
import FeedScreenTabIcon from '../screens/shared/FeedScreenTabIcon';
import NotificationScreenTabIcon from '../screens/shared/NotificationScreenTabIcon';

const FeedStack = StackNavigator({

    FeedScreen : {
        screen : FeedScreen ,
    } ,

    CommentsScreen : {
        screen : CommentsScreen
    } ,

    CreatePostScreen : {
        screen : CreatePostScreen
    } ,

    EditPostScreen : {
        screen : EditPostScreen
    } ,

    PostScreen : {
        screen : PostScreen
    } ,

    ProfileScreen: {
        screen : ProfileScreen
    }

} , {
    navigationOptions : {
        headerStyle : {
            backgroundColor: theme.primaryColor ,
        } ,
        headerTintColor : '#fff' ,
        tabBarIcon : ({ tintColor }) => {
            return <FeedScreenTabIcon tintColor={tintColor} />
        }
    }
});

const NotificationsStack = StackNavigator({

    NotificationsScreen : {
        screen : NotificationsScreen ,
    } ,

    PostScreen: {
        screen : PostScreen
    } ,

    EditPostScreen : {
        screen : EditPostScreen
    } ,

    CommentsScreen : {
        screen : CommentsScreen
    } ,

} , {
    navigationOptions : {
        headerStyle : {
            backgroundColor: theme.primaryColor ,
        } ,
        headerTintColor : '#fff' ,
        tabBarIcon : ({ tintColor }) => {
            return <NotificationScreenTabIcon tintColor={tintColor} notificationsCount={25} />
        }
    }
});

const ProfileStack = StackNavigator({

    ProfileScreen: {
        screen : ProfileScreen
    } ,

    PostScreen : {
        screen : PostScreen
    } ,

    EditPostScreen : {
        screen : EditPostScreen
    } ,

    CommentsScreen : {
        screen : CommentsScreen
    } ,

    EditProfileScreen : {
        screen : EditProfileScreen
    }

} , {
    initialRouteParams : {
        fromDrawer : true ,
        user : null
    } ,
    navigationOptions : {
        headerStyle : {
            backgroundColor: theme.primaryColor ,
        } ,
        headerTintColor : '#fff' ,
    }
});

const SavedPostsStack = StackNavigator({

    SavedPostsScreen: {
        screen : SavedPostsScreen
    } ,

    PostScreen : {
        screen : PostScreen
    } ,

    CommentsScreen : {
        screen : CommentsScreen
    }

} , {
    navigationOptions : {
        headerStyle : {
            backgroundColor: theme.primaryColor ,
        } ,
        headerTintColor : '#fff' ,
    }
});

const HomeTabs = TabNavigator({

    FeedStack : {
        screen : FeedStack
    } ,

    NotificationStack : {
        screen : NotificationsStack
    }

} , {
    tabBarPosition : 'bottom' ,
    swipeEnabled : false ,
    animationEnabled : false ,
    tabBarOptions : {
        style : {
            backgroundColor: theme.primaryColor
        } ,
        showIcon : true ,
        showLabel : false ,
        activeTintColor : '#fff' ,
        indicatorStyle : {
            width: 0
        }
    }
});

const AuthNavigator = StackNavigator({

    SigninScreen : {
        screen : SigninScreen
    } ,

    SignupScreen : {
        screen : SignupScreen
    }

} , {

    navigationOptions : {
        header : null
    }

});

const AppNavigator = DrawerNavigator({

    HomeTabsScreen : {
        screen : HomeTabs
    } ,

    ProfileStack : {
        screen : ProfileStack
    } ,

    SavedPostsStack : {
        screen : SavedPostsStack
    } ,

    SettingsScreen : {
        screen : SettingsScreen
    }

} , {
    contentComponent : ({ navigation }) => {
        return <DrawerContent navigation={navigation} />
    }
});

const RootNavigator = SwitchNavigator({

    // TestScreen ,
    AuthLoading : AuthLoadingScreen ,
    AuthNavigator ,
    AppNavigator

} , {
    initialRouteName : 'AuthLoading'
});

export default RootNavigator;