import React , { Component } from 'react';
import {
    View ,
    ActivityIndicator ,
    StyleSheet ,
    AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { setAuthUser } from '../store/actions';
import axios from 'axios';

class AuthLoadingScreen extends Component {

    constructor (props) {

        super(props);

        AsyncStorage.getItem('authUser').then((user) => {

            if (user) {

                let userObject = JSON.parse(user);

                this.props.setAuthUser(userObject);

                axios.defaults.params = {api_token : userObject.api_token};

                this.props.navigation.navigate('AppNavigator');

            }else{

                this.props.navigation.navigate('AuthNavigator');

            }

        })

    }

    render () {

        return (

            <View style={styles.container}>

                <ActivityIndicator size={'large'} />

            </View>

        );

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
        alignItems: 'center' ,
        justifyContent : 'center'
    }

});

const mapStateToProps = (state) => {

    return {};

};


const mapDispatchToProps = (dispatch) => {

    return {
        setAuthUser : (user) => dispatch(setAuthUser(user))
    }

};

export default connect(mapStateToProps , mapDispatchToProps)(AuthLoadingScreen);