import React , { Component } from 'react';
import {
    View,
    StyleSheet ,
    FlatList ,
    TouchableOpacity ,
    TextInput ,
    Dimensions ,
    ActivityIndicator ,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import CommentItem from './components/CommentItem';
import Icon from 'react-native-vector-icons/FontAwesome';
import theme from '../utils/theme';
import { updateCommentInput , saveComment , fetchComments } from '../store/actions';
import NoDataText from './shared/NoDataText';
import Divider from './shared/Divider';

class CommentsScreen extends Component {

    static navigationOptions = ({ navigation }) => {

        return {
            title: 'Comments' ,
        }

    };

    constructor (props) {
        super(props);
        this.state = {
            screenWidth : Dimensions.get('window').width ,
        }
    }

    componentDidMount () {

        Dimensions.addEventListener('change' , ({ window }) => {
            this.setState({
                screenWidth : window.width
            });
        });

        this.props.fetchComments(this.props.navigation.state.params.postId);

    }

    render () {

        let postId = this.props.navigation.state.params.postId;

        return (

            <View style={styles.container}>

                {

                    this.props.loadingFetchingComments ?

                        <ActivityIndicator size={'large'}/> :

                        <View>

                            {

                                this.props.comments.length > 0 ?

                                    <FlatList
                                        data={this.props.comments}
                                        keyExtractor={(item , index) => index.toString()}
                                        renderItem={({ item , index }) => {

                                            return (

                                                <View style={{ marginTop : index > 0 ? 15 : 0 }}>

                                                    <CommentItem
                                                        commentData={item}
                                                    />

                                                    <Divider />

                                                </View>

                                            );

                                        }}
                                    /> :

                                    <NoDataText
                                        text={'No comments for this post yet!'}
                                    />

                            }

                        </View>

                }

                <View style={[
                    styles.commentInputContainer ,
                    { width : this.state.screenWidth }
                ]}>

                    <View style={{flexDirection : 'row' , alignItems: 'center'}}>

                        <View style={styles.inputContainer}>

                            <TextInput
                                underlineColorAndroid={'transparent'}
                                placeholder={'Leave a comment'}
                                placeholderColor={'#eee'}
                                multiline={true}
                                onChangeText={(text) => {
                                    this.props.updateCommentInput(text);
                                }}
                                value={this.props.commentInput}
                            />

                        </View>

                        <View style={styles.sendIconContainer}>

                            {

                                this.props.loading ?

                                    <ActivityIndicator/> :

                                    <TouchableOpacity
                                        onPress={() => this.onSaveCommentPress(postId)}
                                        disabled={this.props.commentInput ? false : true}
                                    >

                                        <Icon name={'arrow-right'}
                                              size={25}
                                              color={
                                                  this.props.commentInput ? theme.primaryColor : '#ced3d6'
                                              }
                                        />

                                    </TouchableOpacity>

                            }

                        </View>

                    </View>

                </View>

            </View>
        );

    }

    onSaveCommentPress (postId) {

        if (this.props.commentInput) {

            this.props.saveComment(postId);

        }else{

            Alert.alert(
                'Please fill the comment input!' ,
                ''
            );

        }


    }

    getData () {

        return [
            {
                user: {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                created_at : '5 mins ago' ,
                comment : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
            } ,
            {
                user: {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                created_at : '5 mins ago' ,
                comment : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
            } ,
            {
                user: {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                created_at : '5 mins ago' ,
                comment : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
            } ,
            {
                user: {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                created_at : '5 mins ago' ,
                comment : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
            } ,
            {
                user: {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                created_at : '5 mins ago' ,
                comment : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
            } ,
            {
                user: {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                created_at : '5 mins ago' ,
                comment : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
            } ,
        ]

    }


}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
        paddingHorizontal: 15 ,
        paddingTop : 15 ,
        paddingBottom : 50
    } ,

    commentInputContainer : {
        position : 'absolute' ,
        bottom: 0 ,
        backgroundColor: '#fff' ,
        elevation: 5
    } ,

    inputContainer : {
        flex : 1 ,
    } ,

    sendIconContainer : {
        marginHorizontal: 15
    }

});

const mapStateToProps = (state) => {

    return {
        nav : state.nav ,
        commentInput : state.comment.commentInput ,
        loading : state.comment.loading ,
        loadingFetchingComments : state.comment.loadingFetchingComments ,
        comments : state.comment.comments
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        updateCommentInput : (body) => dispatch(updateCommentInput(body)) ,
        saveComment : (postId) => dispatch(saveComment(postId)) ,
        fetchComments : (postId) => dispatch(fetchComments(postId)) ,
    };

};

export default connect(mapStateToProps , mapDispatchToProps)(CommentsScreen);
