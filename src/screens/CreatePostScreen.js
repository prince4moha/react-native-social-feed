import React , { Component } from 'react';
import {
    View,
    StyleSheet ,
    TouchableOpacity ,
    Text ,
    TextInput ,
    ScrollView ,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import { createPost , updateFormData , pickImage } from '../store/actions';
import GradientButton from './shared/GradientButton';
import theme from '../utils/theme';
import PreLoader from './shared/PreLoader';
import PostForm from './components/PostForm';


class CreatePostScreen extends Component {

    static navigationOptions = ({ navigation }) => {

        return {
            title: 'Create New Post' ,
        }

    };

    constructor (props) {

        super(props);

        this.state = {
            formData : {
                title : '' ,
                body : '' ,
                imagePath : null
            } ,
            validationErrors : {
                title : '' ,
                body : ''
            }
        };

    }

    componentDidMount () {


    }

    render () {

        return (

            <View style={styles.container}>

                <ScrollView>

                    <PostForm
                        data={{
                            title : '' ,
                            body : '' ,
                            imagePath : null
                        }}
                    />

                    <View style={styles.buttonContainer}>

                        {

                            this.props.loading ?

                                (
                                  <PreLoader/>
                                )
                                :
                                (
                                    <GradientButton
                                        title={'Create Post'}
                                        buttonStyle={{
                                            borderRadius : 4
                                        }}
                                        onPress={() => this.props.createPost()}
                                    />
                                )

                        }

                    </View>

                </ScrollView>

            </View>

        );

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
        padding : 15
    } ,

    textInput : {
        borderWidth: 1 ,
        borderColor : theme.primaryColor ,
        borderRadius : 4 ,
        paddingVertical : 5 ,
        marginTop : 5
    } ,

    uploadImageContainer : {
        alignItems : 'center' ,
        marginVertical : 15
    } ,

    uploadImageButton : {
        alignItems: 'center'
    } ,

    buttonContainer : {
        paddingVertical : 15
    }

});


const mapStateToProps = (state) => {

    return {
        loading : state.post.loading ,
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        createPost : () => dispatch(createPost()) ,
    };

};

export default connect(mapStateToProps , mapDispatchToProps)(CreatePostScreen);
