import React , { Component } from 'react';
import {
    View,
    StyleSheet ,
    TouchableOpacity ,
    Text ,
    TextInput ,
    ScrollView ,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import { createPost , updatePost } from '../store/actions';
import GradientButton from './shared/GradientButton';
import theme from '../utils/theme';
import PreLoader from './shared/PreLoader';
import PostForm from './components/PostForm';


class EditPostScreen extends Component {

    static navigationOptions = ({ navigation }) => {

        return {
            title: 'Update Post' ,
        }

    };

    constructor (props) {

        super(props);

    }

    render () {

        let { id , title , body , image_url } = this.props.navigation.state.params.postData;

        return (

            <View style={styles.container}>

                <ScrollView>

                    <PostForm
                        data={{
                            title ,
                            body ,
                            imagePath : image_url
                        }}
                    />

                    <View style={styles.buttonContainer}>

                        {

                            this.props.loading ?

                                (
                                    <PreLoader/>
                                )
                                :
                                (
                                    <GradientButton
                                        title={'Update Post'}
                                        buttonStyle={{
                                            borderRadius : 4
                                        }}
                                        onPress={() => this.props.updatePost(id)}
                                    />
                                )

                        }

                    </View>

                </ScrollView>

            </View>

        );

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
        padding : 15
    } ,

    textInput : {
        borderWidth: 1 ,
        borderColor : theme.primaryColor ,
        borderRadius : 4 ,
        paddingVertical : 5 ,
        marginTop : 5
    } ,

    uploadImageContainer : {
        alignItems : 'center' ,
        marginVertical : 15
    } ,

    uploadImageButton : {
        alignItems: 'center'
    } ,

    buttonContainer : {
        paddingVertical : 15
    }

});


const mapStateToProps = (state) => {

    return {
        loading : state.post.loading ,
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        updatePost : (postId) => dispatch(updatePost(postId)) ,
    };

};

export default connect(mapStateToProps , mapDispatchToProps)(EditPostScreen);
