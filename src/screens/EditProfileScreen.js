import React , { Component } from 'react';
import {
    View,
    StyleSheet ,
    ScrollView ,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import { setProfileFormData , updateProfileFormData , updateProfile } from '../store/actions';
import TextInput from './shared/TextInput';
import GradientButton from './shared/GradientButton';
import PreLoader from './shared/PreLoader';
import theme from '../utils/theme';
import ImagePicker from 'react-native-image-crop-picker';

class EditProfileScreen extends Component {

    static navigationOptions = ({ navigation }) => {

        return {
            title: 'Edit Profile' ,
        }

    };

    componentDidMount () {

        let { name , email , profile_image_url } = this.props.authUser;

        this.props.setProfileFormData({
            name ,
            email ,
            profile_image : {
                uri : profile_image_url ,
                name : 'profile_image.jpg' ,
                type : 'image/jpg'
            }
        });

    }

    render () {

        let {name , email , profile_image} = this.props.profileFormData;

        return (

            <View
                style={styles.container}
            >

                <ScrollView
                    contentContainerStyle={styles.scrollViewContentContainer}
                >

                    <View
                        style={styles.inputContainer}
                    >
                        <TextInput
                            placeholder={'name'}
                            value={name}
                            onChangeText={(text) => {
                                this.props.updateProfileFormData('name' , text);
                            }}
                        />
                    </View>

                    <View
                        style={styles.inputContainer}
                    >
                        <TextInput
                            placeholder={'Email'}
                            value={email}
                            onChangeText={(text) => {
                                this.props.updateProfileFormData('email' , text);
                            }}
                        />
                    </View>

                    <View
                        style={styles.profileImageContainer}
                    >

                        <Image
                            style={styles.profileImage}
                            source={{ uri : profile_image.uri }}
                        />

                        <View
                            style={styles.buttonsContainer}
                        >

                            <GradientButton
                                firstColor={theme.primaryColor}
                                secondColor={theme.secondaryColor}
                                title={'Change Image'}
                                containerStyle={styles.button}
                                onPress={() => {
                                    this.pickImage()
                                }}
                            />

                            {

                                profile_image.uri !== 'http://50.116.109.135/~api/social_feed/public/storage/user/profile_image/avatar.jpg' &&

                                <GradientButton
                                    firstColor={theme.primaryDanger}
                                    secondColor={theme.secondaryDanger}
                                    title={'Delete Image'}
                                    containerStyle={styles.button}
                                    onPress={() => {
                                        this.deleteProfileImage()
                                    }}
                                />

                            }

                        </View>

                    </View>

                    <View>

                        {
                            this.props.loadingUpdatingProfileFormData ?

                                <PreLoader /> :

                                <GradientButton
                                    firstColor={theme.primaryColor}
                                    secondColor={theme.secondaryColor}
                                    title={'Update Profile'}
                                    onPress={() => {
                                        this.props.updateProfile();
                                    }}
                                />
                        }

                    </View>

                </ScrollView>

            </View>

        );

    }


    pickImage () {

        ImagePicker.openPicker({
            width : 200 ,
            height : 200
        }).then((image) => {
            this.props.updateProfileFormData('profile_image' , {
                uri : image.path ,
                name : 'profile_image.jpg' ,
                type : 'image/jpg'
            });
        }).catch((err) => {
            console.log(err)
        });

    }

    deleteProfileImage () {

        this.props.updateProfileFormData('profile_image' , {
            uri : 'http://50.116.109.135/~api/social_feed/public/storage/user/profile_image/avatar.jpg' ,
            name : 'profile_image.jpg' ,
            type : 'image/jpg'
        });

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1
    } ,

    scrollViewContentContainer : {
        padding : 10
    } ,

    inputContainer : {
        marginVertical : 10
    } ,

    profileImage : {
        width : 150 ,
        height: 150 ,
        marginBottom: 10
    } ,

    profileImageContainer: {
        alignItems: 'center' ,
        paddingVertical : 20
    } ,

    buttonsContainer : {
        flexDirection : 'row'
    } ,

    button : {
        marginHorizontal : 2
    }

});


const mapStateToProps = (state) => {

    return {
        authUser : state.auth.user ,
        profileFormData : state.profile.profileFormData ,
        loadingUpdatingProfileFormData : state.profile.loadingUpdatingProfileFormData
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        setProfileFormData : (formData) => dispatch(setProfileFormData(formData)) ,
        updateProfileFormData : (key , value) => dispatch(updateProfileFormData(key , value)) ,
        updateProfile : () => dispatch(updateProfile()) ,
    };

};

export default connect(mapStateToProps , mapDispatchToProps)(EditProfileScreen);
