import React , { Component } from 'react';
import {
    View,
    StyleSheet ,
    FlatList ,
    TouchableOpacity ,
    Text
} from 'react-native';
import { connect } from 'react-redux';
import { getFeedPosts } from '../store/actions';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PostItems from './components/PostItems';
import DrawerButton from './shared/DrawerButton';
import PreLoader from './shared/PreLoader';
import NoDataText from './shared/NoDataText';

class FeedScreen extends Component {

    static navigationOptions = ({ navigation }) => {

        return {

            title : 'Feed' ,
            headerRight : (
                <View style={{ marginRight: 10 }}>
                    <TouchableOpacity onPress={() => {
                        navigation.navigate('CreatePostScreen')
                    }}>
                        <Icon name={'add'} size={30} color={'#fff'} />
                    </TouchableOpacity>
                </View>
            ) ,
            headerLeft : (
                <DrawerButton navigation={navigation} />
            )
        }

    };

    constructor (props) {

        super(props);

    }

    componentDidMount () {

        if (this.props.feedPosts.posts.length === 0) {
            this.props.getFeedPosts(0);
        }

    }

    render () {

        return this.props.loadingFetchingPosts ?
            (
                <PreLoader containerStyle={{ marginTop : 20 }} />
            ) :
            (

                <View style={styles.container}>

                    {

                        this.props.feedPosts.posts.length > 0 ?

                            (
                                <PostItems
                                    posts={this.props.feedPosts.posts}
                                    navigation={this.props.navigation}
                                    collection={'feedPosts'}
                                />
                            ) :

                            (
                                <NoDataText
                                    text={'No posts at the current moment!'}
                                />
                            )

                    }

                </View>

            );

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
    } ,

});

const mapStateToProps = (state) => {

    return {
        nav : state.nav ,
        authUser : state.auth.user ,
        feedPosts : state.post.feedPosts ,
        loadingFetchingPosts : state.post.loading ,
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        getFeedPosts : (page) => dispatch(getFeedPosts(page))
    }

};

export default connect(mapStateToProps , mapDispatchToProps)(FeedScreen);
