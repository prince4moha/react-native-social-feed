import React , { Component } from 'react';
import {
    View ,
    Text ,
    StyleSheet ,
    Image ,
    FlatList
} from 'react-native';
import { connect } from 'react-redux';
import Divider  from './shared/Divider';
import NotificationItem from './components/NotificationItem';

class NotificationsScreen extends Component {

    static navigationOptions = ({ navigation }) => {

        return {
            title : 'Notifications' ,
            headerMode : 'screen'
        }

    };

    constructor (props) {
        super(props);
    }

    componentDidMount () {

        console.log(this.props.nav);

    }

    render () {

        return (

            <View style={styles.container}>

                <FlatList
                    data={this.getData()}
                    keyExtractor={(item , index) => index.toString()}
                    renderItem={({ item , index }) => {

                        return (

                            <View>

                                <NotificationItem
                                    style={{ marginTop : index > 0 ? 15 : 0 }}
                                    notificationData={item}
                                    navigation={this.props.navigation}
                                    onPress={() => this.props.navigation.navigate('PostScreen')}
                                />

                                <Divider />

                            </View>

                        );

                    }}
                />

            </View>

        );

    }

    getData () {

        return [
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'comment' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'like' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'comment' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'comment' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'like' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'comment' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'comment' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'like' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'comment' ,
                created_at : '5 mins ago'
            } ,
            {
                user : {
                    name : 'Mohamed Moussa' ,
                    imgUrl : require('../assets/freelancer.jpg')
                } ,
                post : {
                    title : 'Hello World'
                } ,
                type : 'comment' ,
                created_at : '5 mins ago'
            } ,
        ];

    }

}

const styles = StyleSheet.create({

    container : {
        padding: 15 ,
        flex : 1
    } ,

});

const mapStateToProps = (state) => {

    return {
        nav : state.nav
    }

};

export default connect(mapStateToProps)(NotificationsScreen);
