import React , { Component } from 'react';
import {
    View ,
    Text ,
    StyleSheet ,
    Image ,
    ScrollView ,
    TouchableOpacity ,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import {deletePost, saveComment} from '../store/actions';
import theme from '../utils/theme';
import PostActions from './components/PostActions';
import Icon from 'react-native-vector-icons/FontAwesome';
import ActionButton from 'react-native-action-button';
import Divider from './shared/Divider';


class PostScreen extends Component {

    static navigationOptions  = ({ navigation }) => {

        let postData = navigation.state.params.postData;

        return {
            title : postData.title ,
        }

    };

    constructor (props) {
        super(props);
    }

    render () {

        let postData = this.props.currentViewedPost;

        let { collection , index } = this.props.navigation.state.params;

        return (

            <View style={styles.container}>

                <ScrollView>

                    <View>

                        <View style={styles.thumbnailContainer}>

                            <Image
                                style={styles.thumbnailImage}
                                source={{ uri : postData.user.profile_image_url }}
                            />

                            <View>

                                <TouchableOpacity
                                    onPress={this.getToUserProfileScreen.bind(this)}
                                >
                                    <Text style={styles.userName}>
                                        { postData.user.name }
                                    </Text>
                                </TouchableOpacity>

                                <Text>
                                    { postData.created_at }
                                </Text>

                            </View>

                        </View>

                    </View>

                    <Divider style={{ backgroundColor: '#eee' }} />

                    <View style={styles.postImageContainer}>

                        {

                            postData.image_url ?

                                <Image
                                    style={styles.postImage}
                                    source={{ uri : postData.image_url }}
                                /> :

                                null

                        }

                    </View>

                    <View style={styles.titleContainer}>

                        <Text style={styles.titleText}>
                            { postData.title }
                        </Text>

                    </View>

                    <View style={styles.postActionsContainer}>

                        <PostActions
                            postData={postData}
                            collection={collection}
                            index={index}
                        />

                    </View>

                    <View style={styles.bodyContainer}>

                        <Text>

                            { postData.body }

                        </Text>

                    </View>

                </ScrollView>

                {

                    postData.user.id === this.props.authUser.id &&

                    <ActionButton
                        buttonColor={theme.primaryColor}
                        renderIcon={() => <Icon name={'edit'} />}
                    >
                        <ActionButton.Item
                            buttonColor={theme.primaryDanger}
                            title="Delete"
                            onPress={(() => this.deletePost(postData.id))}
                        >
                            <Icon name="trash" />
                        </ActionButton.Item>

                        <ActionButton.Item
                            buttonColor={theme.secondaryColor}
                            title="Edit"
                            onPress={() => this.props.navigation.navigate('EditPostScreen' , {
                                postData : postData
                            })}
                        >
                            <Icon name="edit" />
                        </ActionButton.Item>

                    </ActionButton>

                }

            </View>

        );

    }

    getToUserProfileScreen () {

        this.props.navigation.navigate('ProfileScreen');

    }

    deletePost (postId) {

        Alert.alert(
            'Do you want to delete the post ?!' ,
            '' ,
            [
                { text : 'Yes' , onPress : () => this.props.deletePost(postId) } ,
                { text : 'Cancel' , onPress : () => {} } ,
            ]
        );

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
        backgroundColor: '#fff'
    } ,

    thumbnailContainer : {
        flexDirection: 'row' ,
        alignItems: 'center' ,
        padding : 15
    } ,

    thumbnailImage : {
        width : 50 ,
        height : 50 ,
        borderRadius : 50 ,
        marginRight: 10
    } ,

    userName : {
        fontWeight: 'bold'
    } ,

    postImageContainer : {

    },

    postImage : {
        width: '100%' ,
        height : 200
    } ,

    titleContainer : {
        padding : 15 ,
        borderLeftColor : theme.primaryColor ,
        borderLeftWidth : 8 ,
        marginVertical : 10
    } ,

    titleText : {
        fontStyle : 'italic' ,
        fontSize : 25
    } ,

    postActionsContainer : {
        borderTopWidth: 1 ,
        borderBottomWidth: 1 ,
        borderColor : '#eee' ,
        paddingVertical : 10
    } ,

    bodyContainer : {
        marginVertical : 10 ,
        paddingHorizontal : 15
    }

});

const mapStateToProps = (state) => {

    return {
        nav : state.nav ,
        currentViewedPost : state.post.currentViewedPost ,
        authUser : state.auth.user
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        deletePost : (postId) => dispatch(deletePost(postId))
    }

};

export default connect(mapStateToProps , mapDispatchToProps)(PostScreen);
