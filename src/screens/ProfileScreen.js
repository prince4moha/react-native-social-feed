import React , { Component } from 'react';
import {
    View ,
    Text ,
    StyleSheet ,
    Image ,
    ScrollView ,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from "react-redux";
import { fetchUserProfile , follow , unFollow } from '../store/actions';
import Divider from './shared/Divider';
import GradientButton from './shared/GradientButton';
import PostItems from './components/PostItems';
import DrawerButton from './shared/DrawerButton';
import PreLoader from './shared/PreLoader';
import NoDataText from './shared/NoDataText';
import theme from '../utils/theme';

const ScreenDivider = () => (
    <Divider
        style={{ backgroundColor: '#eee' }}
    />
);

class ProfileScreen extends Component {

    static navigationOptions = ({ navigation }) => {

        let { fromDrawer } = navigation.state.params;

        let navigationOptions = {
            title :'Profile' ,
        };

        if (fromDrawer) {

            navigationOptions = {
                ...navigationOptions ,
                headerLeft : (
                    <DrawerButton
                        navigation={navigation}
                    />
                ) ,
                headerRight : (
                    <TouchableOpacity
                        style={{ marginHorizontal : 10 }}
                        onPress={() => navigation.navigate('EditProfileScreen')}
                    >
                        <Icon name={'edit'} size={25} color={'#fff'} />
                    </TouchableOpacity>
                )
            }

        }

        return navigationOptions;

    };

    constructor (props) {

        super(props);

    }

    componentDidMount () {

        let { fromDrawer , user } = this.props.navigation.state.params;

        if (fromDrawer) {

            this.fetchUser(this.props.authUser.id);

        }else{

            this.fetchUser(user.id);

        }


    }

    render () {

        let profileData = this.props.profileData;

        console.log('profile data' , profileData);

        if (this.props.loadingFetchingProfileData || !profileData) {

            return <PreLoader />

        }

        return (

            <View style={styles.container}>

                <ScrollView>

                    <View style={styles.headerAreaContainer}>

                        <Image
                            style={styles.profileImage}
                            source={{ uri : profileData.profile_image_url }}
                        />

                        <Text style={styles.username}>
                            { profileData.name }
                        </Text>

                        {
                            profileData.id !== this.props.authUser.id &&
                            (
                                profileData.is_following ?
                                    <GradientButton
                                        firstColor={theme.primaryDanger}
                                        secondColor={theme.secondaryDanger}
                                        title={'Un Follow'}
                                        containerStyle={{
                                            width: '50%'
                                        }}
                                        onPress={() => this.props.unFollow(profileData.id)}
                                    /> :
                                    <GradientButton
                                        firstColor={theme.primaryColor}
                                        secondColor={theme.secondaryColor}
                                        title={'Follow'}
                                        containerStyle={{
                                            width: '50%'
                                        }}
                                        onPress={() => this.props.follow(profileData.id)}
                                    />
                            )
                        }

                    </View>

                    <ScreenDivider />

                    <View style={styles.infoContainer}>

                        <View
                            style={{
                                alignItems: 'center'
                            }}
                        >

                            <Text style={styles.infoNumberText}>
                                { profileData.posts_count }
                            </Text>

                            <Text>
                                Posts
                            </Text>

                        </View>

                        <View
                            style={{
                                alignItems: 'center'
                            }}
                        >

                            <Text style={styles.infoNumberText}>
                                { profileData.followers_count }
                            </Text>

                            <Text>
                                Followers
                            </Text>

                        </View>

                        <View
                            style={{
                                alignItems: 'center'
                            }}
                        >

                            <Text style={styles.infoNumberText}>
                                { profileData.followings_count }
                            </Text>

                            <Text>
                                Followings
                            </Text>

                        </View>

                    </View>

                    <ScreenDivider />

                    <View>

                        {

                            this.props.profilePosts.length > 0 ?
                                <PostItems
                                    posts={this.props.profilePosts}
                                    navigation={this.props.navigation}
                                    collection={'profilePosts'}
                                /> :
                                <NoDataText
                                    text={'User has no posts!'}
                                />

                        }

                    </View>

                </ScrollView>

            </View>

        );

    }

    fetchUser (id) {

        this.props.fetchUserProfile(id);

    }


}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
        backgroundColor: '#fff'
    } ,

    headerAreaContainer : {
        alignItems: 'center' ,
        justifyContent : 'center' ,
        padding : 20 ,
        marginBottom : 10
    } ,

    profileImage : {
        width: 100 ,
        height : 100 ,
        borderRadius : 100 ,
        marginBottom : 10
    } ,

    username : {
        fontWeight : 'bold' ,
        marginBottom : 10 ,
        fontSize: 25
    } ,

    infoContainer : {
        marginVertical : 10 ,
        flexDirection : 'row' ,
        justifyContent : 'space-around'
    } ,

    infoNumberText : {
        fontWeight: 'bold' ,
        fontSize: 20
    } ,

    postItemContainer : {
        padding : 10
    }

});

const mapStateToProps = (state) => {

    return {
        authUser : state.auth.user ,
        profileData : state.profile.profileData ,
        loadingFetchingProfileData : state.profile.loadingFetchingProfileData ,
        profilePosts : state.post.profilePosts.posts
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        fetchUserProfile : (id) => dispatch(fetchUserProfile(id)) ,
        follow : (id) => dispatch(follow(id)) ,
        unFollow : (id) => dispatch(unFollow(id)) ,
    }

};

export default connect(mapStateToProps , mapDispatchToProps)(ProfileScreen);