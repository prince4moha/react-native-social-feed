import React , { Component } from 'react';
import {
    View ,
    StyleSheet ,
    TouchableOpacity
} from 'react-native';
import {connect} from "react-redux";
import {getFeedPosts, getSavedPosts} from '../store/actions';
import PostItems from './components/PostItems';
import PreLoader from './shared/PreLoader';
import NoDataText from './shared/NoDataText';
import DrawerButton from './shared/DrawerButton';

class SavedPostsScreen extends Component {

    static navigationOptions = ({ navigation }) => {

        return {

            title : 'Saved Posts' ,
            headerLeft : (
                <DrawerButton navigation={navigation} />
            )

        }

    };

    constructor (props) {

        super(props);

    }

    componentDidMount () {

        if (this.props.savedPosts.posts.length == 0) {
            this.props.getSavedPosts(0);
        }

    }

    render () {

        return this.props.loadingFetchingPosts ?
            (
                <PreLoader containerStyle={{ marginTop : 20 }} />
            ) :
            (

                <View style={styles.container}>

                    {

                        this.props.savedPosts.posts.length > 0 ?

                            (
                                <PostItems
                                    posts={this.props.savedPosts.posts}
                                    navigation={this.props.navigation}
                                    collection={'savedPosts'}
                                />
                            ) :

                            (
                                <NoDataText
                                    text={'You have no saved posts!'}
                                />
                            )

                    }

                </View>

            );

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
    } ,

});

const mapStateToProps = (state) => {

    return {
        nav : state.nav ,
        savedPosts : state.post.savedPosts ,
        loadingFetchingPosts : state.post.loading ,
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        getSavedPosts : (page) => dispatch(getSavedPosts(page))
    }

};

export default connect(mapStateToProps , mapDispatchToProps)(SavedPostsScreen);