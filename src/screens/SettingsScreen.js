import React , { Component } from 'react';
import {
    View ,
    Text
} from 'react-native';
import {connect} from "react-redux";

class SettingsScreen extends Component {

    constructor (props) {
        super(props);
    }

    render () {

        return (
            <View>
                <Text>
                    Settings Screen
                </Text>
            </View>
        );

    }

}

const mapStateToProps = (state) => {

    return {
        nav : state.nav
    }

};

export default connect(mapStateToProps)(SettingsScreen);