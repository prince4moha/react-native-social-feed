import React , { Component } from 'react';
import {
    View ,
    Text ,
    TouchableOpacity ,
    TextInput ,
    ImageBackground ,
    StyleSheet ,
    AsyncStorage
} from 'react-native';
import GradientButton from './shared/GradientButton';
import PreLoader from './shared/PreLoader';
import ErrorMessage from './shared/ErrorMessage';
import { connect } from 'react-redux';
import { validate } from 'validate.js';
import axios from 'axios';
import { signIn , signOut } from '../store/actions';

class SigninScreen extends Component{

    constructor (props) {

        super(props);

        this.state = {
            formData : {
                email : '' ,
                password : ''
            } ,
            validationErrors: {
                email : '' ,
                password : ''
            } ,
        };

    }

    render () {

        return (

            <View style={styles.container}>

                <ImageBackground
                    style={styles.backgroundImage}
                    source={require('../assets/auth-background.jpg')}
                >

                    <View style={styles.innerContainer}>

                        <View>

                            <View style={styles.inputContainer}>

                                <TextInput
                                    style={styles.textInput}
                                    placeholder={'Email'}
                                    placeholderTextColor={'#fff'}
                                    underlineColorAndroid={'transparent'}
                                    onChangeText={(text) => {
                                        this.updateFormData('email' , text);
                                    }}
                                />

                                <ErrorMessage>
                                    { this.state.validationErrors.email }
                                </ErrorMessage>

                            </View>

                            <View style={styles.inputContainer}>

                                <TextInput
                                    style={styles.textInput}
                                    placeholder={'Password'}
                                    placeholderTextColor={'#fff'}
                                    underlineColorAndroid={'transparent'}
                                    secureTextEntry
                                    onChangeText={(text) => {
                                        this.updateFormData('password' , text);
                                    }}
                                />

                                <ErrorMessage>
                                    { this.state.validationErrors.password }
                                </ErrorMessage>

                            </View>

                        </View>

                        {

                            this.props.loading ?

                                <PreLoader/> :

                                <View>

                                    {this.renderServerErrors()}

                                    <GradientButton
                                        onPress={this.submit.bind(this)}
                                        title={'SIGN IN'}
                                        buttonStyle={styles.loginButtonLinearGradient}
                                    />

                                </View>

                        }

                        <View style={styles.newAccountContainer}>

                            <Text style={styles.newAccountText}>
                                Don't have an account!
                            </Text>

                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate('SignupScreen')
                                }}
                            >

                                <Text style={[
                                    styles.newAccountTextButton ,
                                    styles.newAccountText
                                ]}>
                                    Sign up now
                                </Text>

                            </TouchableOpacity>

                        </View>

                    </View>

                </ImageBackground>

            </View>
        );

    }

    updateFormData (key , value) {

        this.setState({
            formData : {
                ...this.state.formData ,
                [key] : value
            }
        });

    }

    submit () {

        this.setState({
            validationErrors : {
                email : '' ,
                password : ''
            }
        });

        if (!this.validateFormData()) {

            this.props.signIn(this.state.formData);

        }

    }
    
    validateFormData () {
        
        const validationResult = validate(this.state.formData , {
            
            email : {
                presence : {
                    allowEmpty : false
                } ,
                email : true
            } ,
            
            password : {
                presence: {
                    allowEmpty: false
                }
            }
            
        });
        
        if (validationResult) {
            
            this.setState({
                validationErrors : {
                    ...this.state.validationErrors ,
                    email : validationResult.email ? validationResult.email[0] : '' ,
                    password : validationResult.password ? validationResult.password[0] : '' ,
                }
            });
            
        }

        return validationResult;
        
    }

    renderServerErrors () {

        if (this.props.serverErrors.length > 0) {

            return this.props.serverErrors.map((error , index) => {

                return (

                    <ErrorMessage key={index.toString()}>
                        { error }
                    </ErrorMessage>

                );

            });

        }else{

            return (
                <View>

                </View>
            );

        }

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1
    } ,

    backgroundImage : {
        width : '100%' ,
        height : '100%' ,
    } ,

    innerContainer : {
        flex : 1 ,
        backgroundColor: 'rgba(0 , 0 , 0 , 0.2)' ,
        justifyContent: 'center'
    } ,

    inputContainer : {
        paddingHorizontal : 20 ,
        paddingVertical : 10
    } ,

    textInput : {
        borderColor: '#fff' ,
        borderWidth: 2 ,
        paddingLeft: 10 ,
        borderRadius : 25 ,
        color : '#fff'
    } ,

    loginButton : {
        paddingHorizontal: 20 ,
        paddingVertical: 10
    } ,

    loginButtonLinearGradient : {
        paddingVertical : 15 ,
        borderRadius: 25 ,
        marginHorizontal : 20
    } ,

    loginButtonText : {
        textAlign: 'center' ,
        color : '#fff'
    } ,

    newAccountContainer : {
        paddingVertical : 10 ,
        flexDirection : 'row' ,
        justifyContent: 'center'
    } ,

    newAccountText : {
        color : '#fff' ,
        marginLeft : 4
    } ,

    newAccountTextButton : {
        fontWeight : 'bold'
    }

});

const mapStateToProps = (state) => {

    return {
        serverErrors : state.auth.serverErrors ,
        loading : state.auth.loading
    };

};

const mapDispatchToProps = (dispatch) => {

    return {
        signIn : (user) => dispatch(signIn(user)) ,
    }

};

export default connect(mapStateToProps , mapDispatchToProps)(SigninScreen);