import React , { Component } from 'react';
import {
    View ,
    Text ,
    TouchableOpacity ,
    TextInput ,
    ImageBackground ,
    StyleSheet ,
    Image ,
    ScrollView ,
    AsyncStorage
} from 'react-native';
import GradientButton from './shared/GradientButton';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import { validate } from 'validate.js';
import ErrorMessage from './shared/ErrorMessage';
import PreLoader from './shared/PreLoader';
import { signUp } from '../store/actions';

class SignupScreen extends Component{

    constructor (props) {

        super(props);

        this.state = {
            formData : {
                fullName : '' ,
                email : '' ,
                password : '' ,
                passwordConfirmation : '' ,
                profileImageUri : null ,
            } ,
            validationErrors : {
                fullName : '' ,
                email : '' ,
                password : '' ,
                passwordConfirmation : '' ,
            }
        };

    }

    componentDidMount () {

    }

    render () {

        return (

            <View style={styles.container}>

                <ImageBackground
                    style={styles.backgroundImage}
                    source={require('../assets/auth-background.jpg')}
                >

                    <View style={styles.innerContainer}>

                        <ScrollView contentContainerStyle={styles.scrollViewContentContainer}>

                            <View style={styles.inputContainer}>

                                <TextInput
                                    style={styles.textInput}
                                    placeholder={'Full Name'}
                                    placeholderTextColor={'#fff'}
                                    underlineColorAndroid={'transparent'}
                                    onChangeText={(text) => {
                                        this.updateFormData('fullName' , text)
                                    }}
                                />

                                <ErrorMessage>
                                    { this.state.validationErrors.fullName || '' }
                                </ErrorMessage>

                            </View>

                            <View style={styles.inputContainer}>

                                <TextInput
                                    style={styles.textInput}
                                    placeholder={'Email'}
                                    placeholderTextColor={'#fff'}
                                    underlineColorAndroid={'transparent'}
                                    onChangeText={(text) => {
                                        this.updateFormData('email' , text)
                                    }}
                                />

                                <ErrorMessage>
                                    { this.state.validationErrors.email || '' }
                                </ErrorMessage>

                            </View>

                            <View style={styles.inputContainer}>

                                <TextInput
                                    style={styles.textInput}
                                    placeholder={'Password'}
                                    placeholderTextColor={'#fff'}
                                    underlineColorAndroid={'transparent'}
                                    secureTextEntry
                                    onChangeText={(text) => {
                                        this.updateFormData('password' , text)
                                    }}
                                />

                                <ErrorMessage>
                                    { this.state.validationErrors.password || '' }
                                </ErrorMessage>

                            </View>

                            <View style={styles.inputContainer}>

                                <TextInput
                                    style={styles.textInput}
                                    placeholder={'Confirm Password'}
                                    placeholderTextColor={'#fff'}
                                    underlineColorAndroid={'transparent'}
                                    secureTextEntry
                                    onChangeText={(text) => {
                                        this.updateFormData('passwordConfirmation' , text)
                                    }}
                                />

                                <ErrorMessage>
                                    { this.state.validationErrors.passwordConfirmation || '' }
                                </ErrorMessage>

                            </View>

                            <View>

                                {
                                    this.state.formData.profileImageUri ?
                                        (

                                            <View
                                                style={styles.profileImageContainer}
                                            >
                                                <Image
                                                    style={styles.selectedProfileImage}
                                                    source={{ uri : this.state.formData.profileImageUri }}
                                                />

                                                <TouchableOpacity
                                                    style={styles.removeImageButton}
                                                    onPress={this.removeProfileImage.bind(this)}
                                                >
                                                    <Text
                                                        style={styles.removeImageButtonText}
                                                    >
                                                        Remove Image
                                                    </Text>
                                                </TouchableOpacity>

                                            </View>

                                        ) :
                                        (

                                            <TouchableOpacity
                                                style={styles.uploadProfileImageContainer}
                                                onPress={this.selectProfileImage.bind(this)}
                                            >

                                                <Icon name={'plus-square'} size={50} color={'#eee'} />

                                                <Text style={{color : '#fff'}}>
                                                    Upload profile image
                                                </Text>

                                            </TouchableOpacity>

                                        )
                                }

                            </View>

                            {

                                this.props.loading ?

                                    <PreLoader/> :

                                    <GradientButton
                                        title={'SIGN UP'}
                                        buttonStyle={styles.loginButtonLinearGradient}
                                        onPress={this.submit.bind(this)}
                                    />

                            }

                            <View style={styles.newAccountContainer}>

                                <Text style={styles.newAccountText}>
                                    Already have an account!
                                </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate('SigninScreen')
                                    }}
                                >
                                    <Text style={[
                                        styles.newAccountText ,
                                        styles.newAccountTextButton
                                    ]}>
                                        Sign in now!
                                    </Text>
                                </TouchableOpacity>

                            </View>

                        </ScrollView>

                    </View>

                </ImageBackground>

            </View>
        );

    }

    updateFormData (key , value) {

        this.setState({
            formData : {
                ...this.state.formData ,
                [key] : value
            }
        });

    }

    selectProfileImage () {

        ImagePicker.openPicker({
            width : 400 ,
            height : 400
        }).then((image) => {
            this.setState({
                formData : {
                    ...this.state.formData ,
                    profileImageUri : image.path
                }
            });
        }).catch((err) => {
            console.log('cancelled');
        })

    }

    removeProfileImage () {

        this.setState({
            formData : {
                ...this.state.formData ,
                profileImageUri : null
            }
        });

    }

    submit () {

        this.setState({
            validationErrors : {
                fullName : '' ,
                email : '' ,
                password : '' ,
                passwordConfirmation : ''
            }
        });

        if (!this.validateFormData()) {

            let formData = new FormData();
            let { fullName , email , password , profileImageUri } = this.state.formData;

            formData.append('name' , fullName);
            formData.append('email' , email);
            formData.append('password' , password);
            formData.append('profile_image' , profileImageUri ? {
                uri : this.state.profileImageUri ,
                name : 'profile_image.jpg' ,
                type : 'image/jpg'
            } : null);

            console.log('sign up form data' , formData);

            this.props.signUp(formData);

        }

    }

    validateFormData () {

        let validationResult = validate( this.state.formData ,
            {
                fullName : {
                    presence : {
                        allowEmpty : false
                    } ,
                } ,
                email : {
                    presence: {
                        allowEmpty: false
                    } ,
                    email : true
                } ,
                password : {
                    presence : {
                        allowEmpty : false
                    }
                } ,
                passwordConfirmation : {
                    presence: {
                        allowEmpty : false ,
                    } ,
                    equality : 'password'
                }
            }
        );

        if (validationResult) {

            this.setState({
                validationErrors : {
                    ...this.state.validationErrors ,
                    fullName : validationResult.fullName ? validationResult.fullName[0] : '' ,
                    email : validationResult.email ? validationResult.email[0] : '' ,
                    password : validationResult.password ? validationResult.password[0] : '' ,
                    passwordConfirmation : validationResult.passwordConfirmation ? validationResult.passwordConfirmation[0] : '' ,
                }
            });

        }

        return validationResult;

    }

    stopLoading () {

        this.setState({
            loading : false
        });

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1
    } ,

    backgroundImage : {
        width : '100%' ,
        height : '100%' ,
    } ,

    innerContainer : {
        flex : 1 ,
        backgroundColor: 'rgba(0 , 0 , 0 , 0.3)' ,
        justifyContent: 'center'
    } ,

    scrollViewContentContainer : {
       paddingTop : 20
    } ,

    inputContainer : {
        paddingHorizontal : 20 ,
        paddingVertical : 10
    } ,

    textInput : {
        borderColor: '#fff' ,
        borderWidth: 2 ,
        paddingLeft: 10 ,
        borderRadius : 25 ,
        color : '#fff'
    } ,

    loginButton : {
        paddingHorizontal: 20 ,
        paddingVertical: 10
    } ,

    loginButtonLinearGradient : {
        paddingVertical : 15 ,
        borderRadius: 25 ,
        marginHorizontal : 20
    } ,

    loginButtonText : {
        textAlign: 'center' ,
        color : '#fff'
    } ,

    newAccountContainer : {
        paddingVertical : 10 ,
        flexDirection : 'row' ,
        justifyContent: 'center'
    } ,

    newAccountText : {
        color : '#fff' ,
        marginLeft : 4
    } ,

    newAccountTextButton : {
        fontWeight : 'bold'
    } ,

    uploadProfileImageContainer : {
        marginVertical: 10 ,
        alignItems: 'center'
    } ,

    selectedProfileImage : {
        width : 100 ,
        height : 100 ,
        borderRadius : 100
    } ,

    profileImageContainer : {
        flexDirection : 'row' ,
        justifyContent : 'center' ,
        alignItems : 'center' ,
        marginVertical: 10
    } ,

    removeImageButton : {
        borderWidth: 1 ,
        borderColor: '#fff' ,
        paddingHorizontal : 10 ,
        paddingVertical : 5 ,
        borderRadius : 20 ,
        marginLeft : 10
    } ,

    removeImageButtonText : {
        color : '#fff'
    }

});

const mapStateToProps = (state) => {

    return {
        loading : state.auth.loading
    };

};

const mapDispatchToProps = (dispatch) => {

    return {
        signUp : (formData) => dispatch(signUp(formData)) ,
    }

};

export default connect(mapStateToProps , mapDispatchToProps)(SignupScreen);