import React , { Component } from 'react';
import {
    View ,
    Text ,
    Button ,
    Alert ,
    FlatList ,
    Image ,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import _ from "lodash";

class TestScreen extends Component{

    componentDidMount () {

        let persons = [
            {name: 'mohamed', age: 15},
            {name : 'taha', age: 20},
            {name: 'yasser', age: 16}
        ];

        const collections = [
            'feedPosts' , 'savedPosts'
        ];

        let newCollection = collections.map((collection) => {

            return {
                [collection] : {
                    posts : {
                        $set : 'Hello World'
                    }
                }
            };

        });

        console.log(
            _.reduce(newCollection , (result , value) => {
                return _.merge(result , value);
            })
        );

    }

    render () {

        let { width , height } = Dimensions.get('window');

        return (

            <View style={{
                flex : 1
            }}>

                {/*<FlatList*/}
                    {/*horizontal*/}
                    {/*data={[*/}
                        {/*require('../assets/freelancer.jpg') ,*/}
                        {/*require('../assets/freelancer.jpg') ,*/}
                        {/*require('../assets/freelancer.jpg') ,*/}
                        {/*require('../assets/freelancer.jpg') ,*/}
                        {/*require('../assets/freelancer.jpg') ,*/}
                        {/*require('../assets/freelancer.jpg') ,*/}
                    {/*]}*/}
                    {/*renderItem={({ item , index }) => {*/}

                        {/*return (*/}
                            {/*<View*/}
                                {/*style={{*/}
                                    {/*width : width / 3 ,*/}
                                    {/*height : height / 3 ,*/}
                                    {/*paddingHorizontal : 5 ,*/}
                                    {/*paddingVertical : 5*/}
                                {/*}}*/}
                            {/*>*/}
                                {/*<Image*/}
                                    {/*style={{*/}
                                        {/*width : '100%' ,*/}
                                        {/*height : '100%' ,*/}
                                    {/*}}*/}
                                    {/*source={item}*/}
                                {/*/>*/}
                            {/*</View>*/}
                        {/*);*/}

                    {/*}}*/}
                    {/*keyExtractor={(item , index) => index.toString()}*/}
                {/*/>*/}

                <View style={{
                    flexDirection : 'row' ,
                    flexWrap : 'wrap'
                }}>

                    <View style={{ width : width / 3 , height : height / 3 , backgroundColor : 'red'}} />
                    <View style={{ width : width / 3 , backgroundColor : 'blue'}} >
                        <Text>Hello World</Text>
                    </View>
                    <View style={{ width : width / 3 , backgroundColor : 'yellow'}} >
                        <Text>Hello World</Text>
                    </View>
                    <View style={{ width : width / 3 , backgroundColor : 'green'}} >
                        <Text>Hello World</Text>
                    </View>
                    <View style={{ width : width / 3 , backgroundColor : 'orange'}} >
                        <Text>Hello World</Text>
                    </View>

                </View>

            </View>

        );

    }

}

const mapStateToProps = (state) => {
    return {}
};

export default connect(mapStateToProps)(TestScreen);