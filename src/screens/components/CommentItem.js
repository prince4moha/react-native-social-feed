import React , { Component } from 'react';
import {
    View ,
    Text ,
    StyleSheet ,
    Image ,
    ImageBackground ,
    TouchableOpacity
} from 'react-native';

export default class CommentItem extends Component {

    constructor (props) {
        super(props);
    }

    render () {

        return (

            <View>

                <View style={[ styles.commentContainer ]}>

                    <View>
                        <Image
                            style={styles.commentUserImage}
                            source={{ uri : this.props.commentData.user.profile_image_url }}
                        />
                    </View>

                    <View style={styles.commentBodyContainer}>

                        <View style={styles.commentNameTimeRow}>

                            <Text style={styles.commentUserName}>
                                {this.props.commentData.user.name}
                            </Text>

                            <Text>
                                {this.props.commentData.created_at}
                            </Text>

                        </View>

                        <View>
                            <Text>
                                {this.props.commentData.body}
                            </Text>
                        </View>

                    </View>

                </View>

            </View>
            
        );

    }

}

const styles = StyleSheet.create({
    commentContainer : {
        flexDirection : 'row' ,
        marginBottom: 15
    } ,

    commentBodyContainer : {
        flex : 1 ,
        marginLeft: 15
    } ,

    commentNameTimeRow : {
        flexDirection: 'row' ,
        justifyContent : 'space-between' ,
        marginBottom : 5
    } ,

    commentUserImage : {
        width: 30 ,
        height: 30 ,
        borderRadius : 30 ,
    } ,

    commentUserName : {
        fontWeight: 'bold'
    }
});
