import React , { Component } from 'react';
import {
    View ,
    Text ,
    StyleSheet ,
    Image ,
    TouchableOpacity
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import Divider from '../shared/Divider';
import Icon from 'react-native-vector-icons/FontAwesome';
import theme from '../../utils/theme';
import ProfileScreen from "../ProfileScreen";
import { connect } from 'react-redux';
import { signOut } from '../../store/actions';

class DrawerContent extends Component {

    render () {

        const ScreenDivider = () => (
            <Divider style={{ backgroundColor : '#fff' }}/>
        );

        const ScreenIcon = (props) => (
            <Icon name={props.name} color={'#fff'} size={20} style={{ marginRight : 10 }} />
        );

        return (

            <View style={{ flex : 1 }}>

                <LinearGradient
                    start={{x: 0.0, y: 0.0}} end={{x: 1.0 , y: 1.0}}
                    colors={[theme.primaryColor , theme.secondaryColor]}
                    style={{flex : 1}}
                >

                    <View style={styles.drawerHeader}>

                        <Image
                            style={{
                                width : 70 ,
                                height: 70 ,
                                borderRadius : 50
                            }}
                            source={{ uri : this.props.authUser.profile_image_url }}
                        />

                        <Text style={styles.text}>
                            { this.props.authUser.name }
                        </Text>

                    </View>

                    <ScreenDivider/>

                    <View>

                        <TouchableOpacity
                            style={styles.buttonContainer}
                            onPress={() => this.props.navigation.navigate('HomeTabsScreen')}
                        >

                            <ScreenIcon name={'home'} />

                            <Text style={styles.buttonText}>
                                Home
                            </Text>

                        </TouchableOpacity>

                        <ScreenDivider/>

                        <TouchableOpacity
                            style={styles.buttonContainer}
                            onPress={() => {
                                this.props.navigation.navigate('ProfileStack');
                            }}
                        >
                            <ScreenIcon name={'user'} />

                            <Text style={styles.buttonText}>
                                Profile
                            </Text>

                        </TouchableOpacity>

                        <ScreenDivider/>

                        <TouchableOpacity
                            style={styles.buttonContainer}
                            onPress={() => this.props.navigation.navigate('SavedPostsStack')}
                        >
                            <ScreenIcon name={'bookmark'} />

                            <Text style={styles.buttonText}>
                                Saved Posts
                            </Text>

                        </TouchableOpacity>

                        <ScreenDivider/>

                        <TouchableOpacity
                            style={styles.buttonContainer}
                            onPress={() => {
                                this.props.signOut();
                            }}
                        >

                            <ScreenIcon name={'sign-out'} />

                            <Text style={styles.buttonText}>
                                Sign out
                            </Text>

                        </TouchableOpacity>

                    </View>


                </LinearGradient>

            </View>

        );

    }

}

const styles = StyleSheet.create({

    drawerHeader : {
        height : 150 ,
        width : '100%' ,
        justifyContent : 'center' ,
        alignItems : 'center'
    } ,

    text : {
        color : '#fff'
    } ,

    buttonContainer : {
        padding: 10 ,
        flexDirection: 'row'
    } ,

    buttonText : {
        color : '#fff' ,
        fontSize : 15
    }

});

const mapStateToProps = (state) => {

    return {
        authUser : state.auth.user
    };

};

const mapDispatchToProps = (dispatch) => {

    return {
        signOut : () => dispatch(signOut())
    };

};

export default connect(mapStateToProps , mapDispatchToProps)(DrawerContent);

