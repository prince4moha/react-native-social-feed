import React , { Component } from 'react';
import {
    View ,
    Text ,
    StyleSheet ,
    Image ,
    ImageBackground ,
    TouchableOpacity
} from 'react-native';

export default class CommentItem extends Component {

    constructor (props) {
        super(props);
    }

    render () {

        return (

            <TouchableOpacity
                style={[
                    {marginBottom : 15 } ,
                    this.props.style
                ]}
                onPress={this.props.onPress}
            >

                <View style={styles.notificationContainer}>

                    <View style={styles.notificationUserImageContainer}>

                        <Image
                            style={styles.notificationUserImage}
                            source={ this.props.notificationData.user.imgUrl }
                        />

                    </View>

                    <View style={styles.notificationBody}>

                        <Text>

                            <Text style={styles.boldText}>
                                { this.props.notificationData.user.name }
                            </Text>

                            <Text> </Text>

                            <Text>
                                { this.getNotificationAction() }
                            </Text>

                            <Text> </Text>

                            <Text style={styles.boldText}>
                                { this.props.notificationData.post.title }
                            </Text>

                        </Text>

                        <Text style={styles.date}>
                            { this.props.notificationData.created_at }
                        </Text>

                    </View>

                </View>

            </TouchableOpacity>

        );

    }

    getNotificationAction () {

        switch (this.props.notificationData.type) {

            case 'comment' :
                return 'commented on your post';

            case 'like' :
                return 'liked your post';

        }

    }

}

const styles = StyleSheet.create({


    notificationContainer : {
        flexDirection: 'row' ,
        alignItems : 'center'
    } ,

    notificationUserImageContainer : {
        marginRight: 10
    } ,

    notificationUserImage : {
        width: 50 ,
        height: 50 ,
        borderRadius : 50 ,
    } ,

    notificationBody : {
        flex : 1
    } ,

    boldText : {
        fontWeight : 'bold'
    } ,

    date : {
        color : '#919191'
    }

});
