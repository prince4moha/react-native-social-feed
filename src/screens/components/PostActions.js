import React from 'react';
import {
    View ,
    StyleSheet ,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import { likePost , dislikePost , savePost , unSavePost } from '../../store/actions';

const PostActions = (props) => {

    return (

        <View style={styles.container}>

            <TouchableOpacity
                onPress={() => {

                    let postData = props.postData;

                    if (postData.liked) {
                        props.dislikePost(postData.id , props.index , props.collection)
                    }else{
                        props.likePost(postData.id , props.index , props.collection)
                    }

                }}
            >
                <Icon
                    name={'heart'}
                    size={25}
                    color={props.postData.liked == true ? 'red' : '#707070'}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={() => {
                props.navigation.navigate('CommentsScreen' , { postId : props.postData.id })
            }}>
                <Icon name={'comments'} size={25} />
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {

                    let postData = props.postData;

                    if (postData.saved) {
                        props.unSavePost(postData.id , props.collection , props.index);
                    }else{
                        props.savePost(postData.id , props.collection , props.index);
                    }

                }}
            >
                <Icon name={'bookmark'}
                      size={25}
                      color={props.postData.saved == true ? 'red' : '#707070'}
                />
            </TouchableOpacity>

        </View>

    );

};

const styles = StyleSheet.create({

    container : {
        flexDirection : 'row' ,
        justifyContent : 'space-around'
    }

});

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch) => {
    return {
        likePost : (postId , index , collection) => dispatch(likePost(postId , index , collection)) ,
        dislikePost : (postId , index , collection) => dispatch(dislikePost(postId , index , collection)) ,
        savePost : (postId , collection , index) => dispatch(savePost(postId , collection , index)) ,
        unSavePost : (postId , collection , index) => dispatch(unSavePost(postId , collection , index)) ,
    };
};

export default connect(mapStateToProps , mapDispatchToProps)(withNavigation(PostActions));