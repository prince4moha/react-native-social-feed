import React , { Component } from 'react';
import {
    View,
    StyleSheet ,
    TouchableOpacity ,
    Text ,
    TextInput ,
    ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import { updateFormData , pickImage , removePickedImage , setFormData } from '../../store/actions';
import Icon from 'react-native-vector-icons/FontAwesome';
import ErrorMessage from '../shared/ErrorMessage';
import theme from '../../utils/theme';

class PostForm extends Component {

    constructor (props) {

        super(props);

    }

    componentWillMount () {

        this.props.setFormData(this.props.data);

    }

    render () {

        let { title , body , imagePath } = this.props.data;

        return (

            <View>

                <View>

                    <TextInput
                        placeholder={'Post Title'}
                        style={styles.textInput}
                        underlineColorAndroid={'transparent'}
                        onChangeText={(text) => this.props.updateFormData('title' , text)}
                        value={this.props.formData.title}
                    />

                    <ErrorMessage>
                        { this.props.validationErrors.title }
                    </ErrorMessage>

                </View>

                <View style={styles.uploadImageContainer}>

                    {

                        this.props.formData.imagePath ?

                            (

                                <ImageBackground
                                    style={{ width: 200 , height:200 }}
                                    source={{ uri : this.props.formData.imagePath }}
                                >

                                    <View style={{ alignItems : 'flex-end' }}>
                                        <TouchableOpacity
                                            style={{ marginRight : 10 }}
                                            onPress={() => this.props.removePickedImage()}
                                        >
                                            <Icon name={'times'} size={30} color={'#fff'}/>
                                        </TouchableOpacity>
                                    </View>

                                </ImageBackground>

                            ) :
                            (

                                <TouchableOpacity
                                    style={styles.uploadImageButton}
                                    onPress={() => this.props.pickImage()}
                                >

                                    <Icon name={'plus-square'} color={theme.primaryColor} size={100}/>

                                    <Text>Upload image with your post</Text>

                                </TouchableOpacity>

                            )

                    }

                </View>

                <View>

                    <TextInput
                        placeholder={'Post Body'}
                        style={styles.textInput}
                        underlineColorAndroid={'transparent'}
                        multiline={true}
                        numberOfLines={5}
                        onChangeText={(text) => this.props.updateFormData('body' , text)}
                        value={this.props.formData.body}
                    />

                    <ErrorMessage>
                        { this.props.validationErrors.body }
                    </ErrorMessage>

                </View>

            </View>

        );

    }

}

const styles = StyleSheet.create({

    container : {
        flex : 1 ,
        padding : 15
    } ,

    textInput : {
        borderWidth: 1 ,
        borderColor : theme.primaryColor ,
        borderRadius : 4 ,
        paddingVertical : 5 ,
        marginTop : 5
    } ,

    uploadImageContainer : {
        alignItems : 'center' ,
        marginVertical : 15
    } ,

    uploadImageButton : {
        alignItems: 'center'
    } ,

    buttonContainer : {
        paddingVertical : 15
    }

});


const mapStateToProps = (state) => {

    return {
        validationErrors : state.post.validationErrors ,
        formData : state.post.formData
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        updateFormData : (key , value) => dispatch(updateFormData(key , value)) ,
        pickImage : (imagePath = null) => dispatch(pickImage(imagePath)) ,
        removePickedImage : () => dispatch(removePickedImage()) ,
        setFormData : (formData) => dispatch(setFormData(formData))
    };

};

export default connect(mapStateToProps , mapDispatchToProps)(PostForm);
