import React , { Component } from 'react';
import {
    View ,
    Text ,
    StyleSheet ,
    Image ,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { setCurrentViewedPost } from '../../store/actions';
import theme from '../../utils/theme';
import PostActions from './PostActions';
import Divider from '../shared/Divider';

class PostItem extends Component {

    constructor (props) {
        super(props);
    }

    render () {

        return (

            <View style={styles.outerContainer}>

                <View style={styles.container}>

                    <View style={styles.thumbnailContainer}>

                        <View style={{ flexDirection: 'row' , alignItems : 'center' }}>

                            <View>

                                <Image
                                    style={styles.thumbnailImage}
                                    source={{
                                        uri : this.props.postData.user.profile_image_url
                                    }}
                                />

                            </View>

                            <View style={styles.thumbnailTextContainer}>

                                <TouchableOpacity
                                    onPress={
                                        () => this.props.navigation.navigate('ProfileScreen' , {
                                            formDrawer : false ,
                                            user : this.props.postData.user ,
                                        })
                                    }
                                >
                                    <Text style={styles.thumbnailUsername}>
                                        {this.props.postData.user.name}
                                    </Text>
                                </TouchableOpacity>

                                <Text>
                                    {this.props.postData.created_at}
                                </Text>

                            </View>

                        </View>

                    </View>

                    <Divider style={{ backgroundColor: '#eee' }} />

                    <View>

                        {

                            this.props.postData.image_url != "" ?

                                <Image
                                    style={{
                                        width : '100%' ,
                                        height: 200 ,
                                    }}
                                    source={{ uri : this.props.postData.image_url }}
                                /> :

                                null

                        }

                    </View>

                    <View style={styles.infoContainer}>

                        <Text style={styles.infoTextTitle}>
                            {this.props.postData.title}
                        </Text>

                        <View
                            style={{
                                flexDirection : 'row' ,
                                alignItems : 'flex-end'
                            }}
                        >

                            <View
                                style={{
                                    flex : 1
                                }}
                            >

                                <Text numberOfLines={2}>
                                    {this.props.postData.body}
                                </Text>

                            </View>

                            <View>
                                <TouchableOpacity
                                    onPress={() => {

                                        this.props.setCurrentViewedPost(this.props.postData);

                                        this.props.navigation.navigate('PostScreen' , {
                                            postData : this.props.postData ,
                                            collection : this.props.collection ,
                                            index : this.props.index
                                        });

                                    }}
                                >
                                    <Text style={{
                                        color : theme.primaryColor
                                    }}>
                                        read more
                                    </Text>
                                </TouchableOpacity>
                            </View>

                        </View>

                    </View>

                    <Divider style={{ backgroundColor: '#eee' }} />

                    <View style={styles.actionButtonsContainer}>

                        <PostActions
                            index={this.props.index}
                            collection={this.props.collection}
                            postData={this.props.postData} />

                    </View>

                </View>

            </View>

        );

    }

    onCommentsButtonPress () {

        this.props.navigation.navigate('CommentsScreen');

    }

}

const styles = StyleSheet.create({

    outerContainer : {
        marginHorizontal : 10 ,
        marginTop : 10 ,
        marginBottom : 5
    } ,

    container : {
        borderColor : '#dfdfdf' ,
        borderWidth : 1 ,
        borderRadius : 8 ,
        backgroundColor: '#fff' ,
        elevation: 3
    } ,

    thumbnailContainer : {
        padding: 10
    } ,

    thumbnailImage : {
        width : 50 ,
        height : 50 ,
        borderRadius: 25
    } ,

    thumbnailTextContainer : {
        marginLeft: 10 ,
    } ,

    thumbnailUsername : {
        fontWeight: 'bold'
    } ,

    actionButtonsContainer : {
        padding: 10 ,
        paddingTop: 15
    } ,

    infoContainer : {
        padding : 10
    } ,

    infoTextTitle : {
        fontWeight: 'bold' ,
        fontSize: 20
    }

});

const mapStateToProps = (state) => {

    return {};

};

const mapDispatchToProps = (dispatch) => {

    return {
        setCurrentViewedPost : (post) => dispatch(setCurrentViewedPost(post))
    }

};

export default connect(mapStateToProps , mapDispatchToProps)(PostItem);