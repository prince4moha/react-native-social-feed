import React , { Component } from 'react';
import {
    View ,
    FlatList ,
    TouchableOpacity ,
    Text
} from 'react-native';
import { connect } from 'react-redux';
import { getFeedPosts , getSavedPosts } from '../../store/actions';
import PostItem from './PostItem';
import PreLoader from '../shared/PreLoader';

class PostItems extends Component {

    constructor (props) {

        super(props);

    }

    render () {

        return (

            <View>

                <FlatList
                    ref={(list) => this.list = list}
                    data={this.props.posts}
                    keyExtractor={(item , index) => index.toString()}
                    onEndReached={this.onEndReached.bind(this)}
                    onEndReachedThreshold={0.2}
                    renderItem={({ item , index }) => {

                        return (

                            <PostItem
                                index={index}
                                postData={item}
                                navigation={this.props.navigation}
                                collection={this.props.collection}
                            />

                        );

                    }}
                />

            </View>

        );

    }

    onEndReached () {

        this.fetchPosts(this.props.collection);

    }

    fetchPosts (collection) {

        switch (collection) {

            case 'feedPosts' :

                this.props.getFeedPosts(this.props.feedPostsPage + 1);

                break;

            case 'savedPosts' :

                this.props.getSavedPosts(this.props.savedPostsPage + 1);

                break;

        }


    }

}

const mapStateToProps = (state) => {

    return {
        feedPostsPage : state.post.feedPosts.page ,
        savedPostsPage : state.post.savedPosts.page ,
        loadingMore : state.post.loadingMore
    };

};

const mapDispatchToProps = (dispatch) => {

    return {
        getFeedPosts : (page) => dispatch(getFeedPosts(page)) ,
        getSavedPosts : (page) => dispatch(getSavedPosts(page)) ,
    };

};

export default connect(mapStateToProps , mapDispatchToProps)(PostItems);