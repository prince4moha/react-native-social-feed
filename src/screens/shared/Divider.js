import React from 'react';
import { View , StyleSheet } from 'react-native';

export default (props) => {

    return (
        <View style={[
            styles.container ,
            props.style
        ]}>

        </View>
    );

}

const styles = StyleSheet.create({

    container : {
        width : '100%' ,
        height: 1 ,
        backgroundColor: '#838383'
    }

});

