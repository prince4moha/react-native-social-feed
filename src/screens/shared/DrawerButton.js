import React , { Component } from 'react';
import {
    View ,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default (props) => {

    return (
        <View style={{
            marginLeft: 10
        }}>
            <TouchableOpacity
                onPress={() => {
                    props.navigation.navigate('DrawerOpen')
                }}
            >
                <Icon name={'bars'} size={25} color={'#fff'}/>
            </TouchableOpacity>
        </View>
    );

}