import React from 'react';
import {
    View ,
    Text ,
    StyleSheet
} from 'react-native';

export default (props) => {

    return props.children ?
    (

        <View style={styles.container}>

            <Text style={styles.text}>
                { props.children }
            </Text>

        </View>

    ) :
    (
        <View>

        </View>
    )

};

const styles = StyleSheet.create({

    container : {
       backgroundColor: '#dc3545' ,
        paddingVertical : 5 ,
        marginVertical: 5 ,
        borderRadius : 8
    } ,

    text : {
        color : '#fff' ,
        textAlign: 'center'
    }

});