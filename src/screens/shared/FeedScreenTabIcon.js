import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default ({ tintColor }) => {

    return (
        <Icon name={'view-list'} size={30} color={tintColor} />
    );

}

