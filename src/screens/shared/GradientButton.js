import React from 'react';
import {
    Text ,
    TouchableOpacity ,
    StyleSheet
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import theme from '../../utils/theme';

export default (props) => {

    let { firstColor , secondColor } = props;

    return (

        <TouchableOpacity
            onPress={props.onPress}
            style={props.containerStyle}
        >
            <LinearGradient
                start={{x: 0.0, y: 0.0}} end={{x: 1.0 , y: 1.0}}
                colors={[firstColor || theme.primaryColor , secondColor || theme.secondaryColor]}
                style={[
                    styles.LinearGradientButton ,
                    props.buttonStyle
                ]}
            >
                <Text
                    style={styles.ButtonText}
                >
                    { props.title }
                </Text>
            </LinearGradient>
        </TouchableOpacity>

    );

}

const styles = StyleSheet.create({

    LinearGradientButton : {
        paddingVertical : 10 ,
        borderRadius: 25 ,
        paddingHorizontal : 10
    } ,

    ButtonText : {
        textAlign: 'center' ,
        color : '#fff'
    }

});