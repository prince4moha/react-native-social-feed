import React from 'react';
import {
    View ,
    Text ,
    StyleSheet
} from 'react-native';

export default (props) => {

    return (

        <View style={styles.container}>
            <Text style={styles.text}>
                { props.text }
            </Text>
        </View>

    );

}

const styles = StyleSheet.create({

    container : {
        alignItems : 'center' ,
        paddingVertical : 10
    } ,

    text : {
        fontSize : 20
    }

});