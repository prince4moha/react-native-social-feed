import React from 'react';
import { View , Text , StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default ({ notificationsCount , tintColor }) => {

    return (
        <View
            style={styles.container}
        >
            <Icon name={'bell'} size={25} color={tintColor} />
            <View style={styles.textContainer}>
                <Text style={styles.notificationsCountText}>
                    { notificationsCount }
                </Text>
            </View>
        </View>
    );

}

const styles = StyleSheet.create({

    container : {
        position : 'relative'
    } ,

    textContainer : {
        backgroundColor : 'red' ,
        borderRadius : 10 ,
        position : 'absolute' ,
        right: 0 ,
        top : 0 ,
        width : 17 ,
        height: 17 ,
        alignItems : 'center' ,
        justifyContent : 'center'
    } ,

    notificationsCountText : {
        color : '#fff' ,
        fontSize : 12
    }

});