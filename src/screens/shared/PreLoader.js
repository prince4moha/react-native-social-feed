import React from 'react';
import {
    View ,
    ActivityIndicator ,
    StyleSheet
} from 'react-native';

export default (props) => {

    return (

        <View style={[
            styles.container ,
            props.containerStyle
        ]}>

            <ActivityIndicator size={'large'} />

        </View>

    );

}

const styles = StyleSheet.create({

    container : {
        alignItems : 'center' ,
        justifyContent : 'center' ,
        marginVertical : 5
    }

});