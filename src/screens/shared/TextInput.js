import React from 'react';
import {
    StyleSheet ,
    TextInput
} from 'react-native';
import theme from "../../utils/theme";

export default (props) => {

    return (

        <TextInput
            placeholder={props.placeholder}
            style={[styles.textInput , props.style]}
            underlineColorAndroid={'transparent'}
            onChangeText={props.onChangeText}
            value={props.value}
        />

    );

}

const styles = StyleSheet.create({

    textInput : {
        borderWidth: 1 ,
        borderColor : theme.primaryColor ,
        borderRadius : 4 ,
        paddingVertical : 5 ,
        marginTop : 5
    }

});