import { NavigationActions } from 'react-navigation';
import { AsyncStorage } from 'react-native';
import axios from "axios/index";

export const signIn = (formData) => {

    return (dispatch) => {

        dispatch(clearServerErrors());

        dispatch(loading(true));

        axios({
            method : 'POST' ,
            url : 'signin' ,
            data : formData
        }).then(({ data }) => {

            if (data.status == false) {

                dispatch({
                    type : 'AUTH_SERVER_ERRORS' ,
                    serverErrors : data.message
                });

                dispatch(loading(false));

            }else{

                AsyncStorage.setItem('authUser' , JSON.stringify(data.data)).then(() => {

                    dispatch(loading(false));

                    dispatch(setAuthUser(data.data));

                    axios.defaults.params = {api_token : data.data.api_token};

                    dispatch(NavigationActions.navigate({
                        routeName : 'AppNavigator'
                    }));

                });

            }

        }).catch((err) => {
            dispatch(loading(false));
            console.log('err' , err);
        });

    }

};

export const signUp = (formData) => {

    return (dispatch) => {

        dispatch(loading(true));

        axios({
            method : 'POST' ,
            url : 'signup' ,
            data : formData ,
        }).then(({ data }) => {

            AsyncStorage.setItem('authUser' , JSON.stringify(data.data)).then(() => {

                dispatch(loading(false));

                dispatch(setAuthUser(data.data));

                axios.defaults.params = {api_token : data.data.api_token};

                dispatch(NavigationActions.navigate({
                    routeName : 'AppNavigator'
                }));

            });

        }).catch((err) => {

            dispatch(loading(false));
            console.log(err);

        });

    }

};

export const signOut = () => {

    return (dispatch) => {

        dispatch(NavigationActions.navigate({
            routeName : 'AuthNavigator' ,
            action : NavigationActions.navigate({ routeName : 'SigninScreen' })
        }));

        AsyncStorage.removeItem('authUser').then(() => {

            dispatch({
                type : 'AUTH_SIGNOUT'
            });

        });

    }

};

export const setAuthUser = (user) => {

    return {
        type : 'AUTH_SET_AUTHUSER' ,
        user
    }

};

export const loading = (status = false) => {

    return {
        type : 'AUTH_LOADING' ,
        status
    }

};

export const clearServerErrors = () => {

    return {
        type : 'AUTH_CLEAR_SERVER_ERRORS' ,
    }

};