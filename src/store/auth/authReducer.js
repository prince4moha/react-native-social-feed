const initialState = {
    user : null ,
    serverErrors : [] ,
    loading : false
};

export default (state = initialState , action) => {

    switch (action.type) {

        case 'AUTH_SIGNOUT' :

            return {
                ...state ,
                user : null
            };

        case 'AUTH_SET_AUTHUSER' :

            return {
                ...state ,
                user : action.user
            };

        case 'AUTH_SERVER_ERRORS' :

            return {
                ...state ,
                serverErrors : action.serverErrors
            };

        case 'AUTH_LOADING' :

            return {
                ...state ,
                loading : action.status
            };

        case 'AUTH_CLEAR_SERVER_ERRORS' :

            return {
                ...state ,
                serverErrors : []
            };

        default :

            return state;

    }

}