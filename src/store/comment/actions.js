import axios from 'axios';

export const updateCommentInput = (body) => {

    return {
        type : 'UPDATE_COMMENT_INPUT' ,
        body
    }

};

const clearCommentInput = () => {

    return {
        type : 'CLEAR_COMMENT_INPUT'
    }

};

export const saveComment = (postId) => {

    return (dispatch , getState) => {

        dispatch(loading(true));

        axios({
            method : 'POST' ,
            url : 'comment/save' ,
            data : {
                body : getState().comment.commentInput ,
                post_id : postId
            }
        }).then(({ data }) => {

            dispatch({
                type : 'COMMENT_SAVED' ,
                comment : data.data
            });

            dispatch(clearCommentInput());
            dispatch(loading(false));

        }).catch((err) => {

            console.log(err);

            dispatch(clearCommentInput());
            dispatch(loading(false));

        });

    };

};

export const fetchComments = (postId) => {

    return (dispatch) => {

        dispatch(loadingFetchingComments(true));

        axios({
            method: 'GET' ,
            url : 'comment/get' ,
            params : {
                post_id : postId
            }
        }).then(({ data }) => {

            dispatch({
                type : 'FETCH_COMMENTS' ,
                comments : data.data
            });

            dispatch(loadingFetchingComments(false));

        }).catch((err) => {

            console.log(err);

            dispatch(loadingFetchingComments(false));

        });

    };

};

const loading = (status) => {

    return {
        type : 'LOADING' ,
        status
    };

};

const loadingFetchingComments = (status) => {

    return {
        type : 'LOADING_FETCHING_COMMENTS' ,
        status
    };

};
