import update from 'immutability-helper';

const initialState = {
    commentInput : '' ,
    comments : [] ,
    loading : false ,
    loadingFetchingComments :false
};

export default (state = initialState , action) => {

    switch (action.type) {

        case 'UPDATE_COMMENT_INPUT' :

            return update(state , {
                commentInput : {
                    $set : action.body
                }
            });

        case 'CLEAR_COMMENT_INPUT' :

            return update(state , {
                commentInput : {
                    $set : ''
                }
            });

        case 'COMMENT_SAVED' :

            return update(state , {
                comments : {
                    $unshift : [action.comment]
                }
            });

        case 'FETCH_COMMENTS' :

            return update(state , {
                comments : {
                    $set : action.comments
                }
            });

        case 'LOADING' :

            return update(state , {
                loading : {
                    $set : action.status
                }
            });

        case 'LOADING_FETCHING_COMMENTS' :

            return update(state , {
                loadingFetchingComments : {
                    $set : action.status
                }
            });

        default :

            return state;

    }

}