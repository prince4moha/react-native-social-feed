import {createStore} from 'redux';
import middleware from './middleware';
import rootReducer from './rootReducer';


const configureStore = () => {

    return createStore(rootReducer , {} , middleware);

};

export default configureStore;