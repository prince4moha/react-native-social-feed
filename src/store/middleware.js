import {createReactNavigationReduxMiddleware} from "react-navigation-redux-helpers";
import ReduxThunk from 'redux-thunk';
import { applyMiddleware } from 'redux';

const reactNavigationReduxMiddleware = createReactNavigationReduxMiddleware(
    'root' ,
    (state) => state.nav
);

export default applyMiddleware(ReduxThunk , reactNavigationReduxMiddleware);