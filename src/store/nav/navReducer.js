import RootNavigator from '../../routes';

const navReducer = (state , action) => {

    const nextState = RootNavigator.router.getStateForAction(action , state);

    return nextState || state;

};

export default navReducer;