import axios from 'axios';
import { NavigationActions } from 'react-navigation';
import { validateFormData } from './helpers';
import ImagePicker from 'react-native-image-crop-picker';

export const getFeedPosts = (page) => {

    return (dispatch) => {

        if (page == 0) {
            dispatch(loading('LOADING_FETCHING_POSTS' , true))
        }else{
            dispatch(loading('LOADING_MORE_POSTS' , true));
        }

        axios({
            method : 'GET' ,
            url : 'post/get-feed-posts' ,
            params : {
                page
            }
        }).then(({ data }) => {

            console.log('feed posts =============> ' , data.data);

            if (page == 0) {
                dispatch(loading('LOADING_FETCHING_POSTS' , false))
            }else{
                dispatch(loading('LOADING_MORE_POSTS' , false));
            }

            dispatch({
                type : 'GET_FEED_POSTS' ,
                posts : data.data ,
                page
            });

        }).catch((err) => {

            dispatch(loading(false));

            console.log(err);

        });

    }

};

export const getSavedPosts = (page) => {

    return (dispatch) => {

        if (page == 0) {
            dispatch(loading('LOADING_FETCHING_POSTS' , true))
        }else{
            dispatch(loading('LOADING_MORE_POSTS' , true));
        }

        axios({
            method : 'GET' ,
            url : 'post/get-saved-posts' ,
            params : {
                page
            }
        }).then(({ data }) => {

            console.log('saved posts =============> ' , data.data);

            if (page == 0) {
                dispatch(loading('LOADING_FETCHING_POSTS' , false))
            }else{
                dispatch(loading('LOADING_MORE_POSTS' , false));
            }

            dispatch({
                type : 'GET_SAVED_POSTS' ,
                posts : data.data ,
                page
            });

        }).catch((err) => {

            dispatch(loading(false));

            console.log(err);

        });

    }

};

export const updateFormData = (key , value) => {

    return {
        type : 'UPDATE_FORM_DATA' ,
        key ,
        value
    };

};

export const pickImage = (imagePath = null) => {

    return (dispatch) => {

        if (imagePath) {

            dispatch({
                type : 'PICK_IMAGE' ,
                imagePath
            });

        }else{

            ImagePicker.openPicker({
                width : 400 ,
                height : 400
            }).then((image) => {

                dispatch({
                    type : 'PICK_IMAGE' ,
                    imagePath : image.path
                });

            }).catch((err) => {
                console.log(err);
            });

        }

    }

};

export const removePickedImage = () => {

    return {
        type : 'REMOVE_PICKED_IMAGE'
    };

};

export const setFormData = (formData) => {

    return {
        type : 'SET_FORM_DATA' ,
        formData
    }

};

export const createPost = () => {

    return (dispatch , getState) => {

        dispatch(loading(true));

        let { title , body , imagePath } = getState().post.formData;

        let formData = new FormData();

        formData.append('title' , title);
        formData.append('body' , body);
        formData.append('image' , imagePath ? {
            uri : imagePath ,
            name : 'image.jpg' ,
            type : 'image/jpg'
        } : null);

        console.log('create post form data' , formData);

        let validationResult = validateFormData(getState().post.formData);

        if (!validationResult) {

            dispatch({ type : 'CLEAR_VALIDATION_ERRORS' });

            axios({
                method : 'POST' ,
                url : 'post/save' ,
                data : formData ,
            }).then(({ data }) => {

                dispatch(loading(false));
                dispatch({
                    type : 'ADD_NEW_POST' ,
                    post : data.data
                });
                dispatch({ type : 'CLEAR_FORM_DATA' });
                dispatch(NavigationActions.back({ key : null }));

            }).catch((err) => {
                dispatch(loading(false));
                console.log(err);
            });

        }else{

            dispatch(loading(false));

            dispatch({
                type : 'FORM_VALIDATION_FAILED' ,
                title : validationResult.title ? validationResult.title[0] : '' ,
                body : validationResult.body ? validationResult.body[0] : '' ,
            });

        }

    }

};

export const updatePost = (postId) => {

    return (dispatch , getState) => {

        dispatch(loading(true));

        let { title , body , imagePath } = getState().post.formData;

        let formData = new FormData();

        formData.append('title' , title);
        formData.append('body' , body);
        formData.append('image_url' , imagePath);
        formData.append('image' , imagePath ? {
            uri : imagePath ,
            name : 'image.jpg' ,
            type : 'image/jpg'
        } : null);

        let validationResult = validateFormData(getState().post.formData);

        if (!validationResult) {

            dispatch({ type : 'CLEAR_VALIDATION_ERRORS' });

            axios({
                method : 'POST' ,
                url : 'post/update' ,
                data : formData ,
                params : {
                    post_id : postId
                }
            }).then(({ data }) => {
                dispatch(loading(false));
                dispatch({
                    type : 'UPDATE_POST' ,
                    post : data.data ,
                    postId
                });
                dispatch({ type : 'CLEAR_FORM_DATA' });
                dispatch(NavigationActions.back({ key : null }));

            }).catch((err) => {
                dispatch(loading(false));
                console.log(err);
            });

        }else{

            dispatch(loading(false));

            dispatch({
                type : 'FORM_VALIDATION_FAILED' ,
                title : validationResult.title ? validationResult.title[0] : '' ,
                body : validationResult.body ? validationResult.body[0] : '' ,
            });

        }

    }

};

export const deletePost = (postId) => {

    return (dispatch) => {

        axios({
            method : 'GET' ,
            url : 'post/delete' ,
            params : {
                post_id : postId
            }
        }).then(({ data }) => {

            if (data['status'] == true) {

                dispatch({
                    type : 'DELETE_POST' ,
                    postId
                });

                dispatch(NavigationActions.back({ key : null }));

            }

        }).catch((err) => {
            console.log(err);
        });

    }

};

export const likePost = (postId , index , collection) => {

    return (dispatch) => {

        dispatch({
            type : 'LIKE_POST' ,
            postId ,
            index ,
            collection
        });

        axios({
            method : 'POST' ,
            url : 'post/like' ,
            data : {
                post_id : postId
            }
        }).then((response) => {
        }).catch((err) => {
            console.log(err);
        });

    };

};

export const dislikePost = (postId , index , collection) => {

    return (dispatch) => {

        dispatch({
            type : 'DISLIKE_POST' ,
            postId ,
            index ,
            collection
        });

        axios({
            method : 'POST' ,
            url : 'post/dislike' ,
            data : {
                post_id : postId
            }
        }).then((response) => {
        }).catch((err) => {
            console.log(err);
        });

    };

};

export const savePost = (postId , collection , index) => {

    return (dispatch) => {

        dispatch({
            type : 'SAVE_POST' ,
            postId ,
            collection ,
            index
        });

        axios({
            method : 'POST' ,
            url : 'post/save' ,
            data : {
                post_id : postId
            }
        }).then(() => {})
            .catch((err) => {
                console.log(err);
            });

    };

};

export const unSavePost = (postId , collection , index) => {

    return (dispatch) => {

        dispatch({
            type : 'UN_SAVE_POST' ,
            postId ,
            collection ,
            index
        });

        axios({
            method : 'POST' ,
            url : 'post/unSave' ,
            data : {
                post_id : postId
            }
        }).then(() => {})
            .catch((err) => {
                console.log(err);
            });

    };
};

export const setCurrentViewedPost = (post) => {

    return {
        type : 'SET_CURRENT_VIEWED_POST' ,
        post
    };

};

export const setProfilePosts = (posts) => {

    return {
        type : 'SET_PROFILE_POSTS' ,
        posts
    }

};

const loading = (type , status) => {

    return {
        type ,
        status
    }

};
