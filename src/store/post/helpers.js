import validate from 'validate.js';
import _ from 'lodash';

export const validateFormData = (formData) => {

    return validate(
        formData ,
        {
            title : {
                presence : {
                    allowEmpty : false
                }
            } ,
            body : {
                presence : {
                    allowEmpty : false
                }
            }
        }
    );

};

export const updatePostInCollections  = (state , collections , postId ,attribute , value) => {

    let newCollection = collections.map((collection) => {

        let postIndexInCollection = state[collection].posts.findIndex((post) => {
            return post.id == postId;
        });

        if (postIndexInCollection !== -1) {

            return {
                [collection] : {
                    posts: {
                        [postIndexInCollection] : {
                            [attribute] : {
                                $set : value
                            }
                        }
                    }
                }
            }

        }

    });

    return _.reduce(newCollection , (result , value) => {

        return _.merge(result , value);

    });

};