import update from 'immutability-helper';
import { updatePostInCollections } from './helpers';
import _ from 'lodash';

const collections = [
    'feedPosts' , 'savedPosts' , 'profilePosts'
];

const initialState = {
    formData : {
        title : '' ,
        body : '' ,
        imagePath : null
    } ,
    validationErrors : {
        title : '' ,
        body : ''
    } ,
    feedPosts : {
        page : 0 ,
        posts : []
    } ,
    savedPosts : {
        page : 0 ,
        posts : [] ,
    } ,
    profilePosts : {
        posts: []
    } ,
    currentViewedPost : null ,
    loading : false ,
    loadingMore : false
};

export default (state = initialState , action) => {

    switch (action.type) {

        case 'UPDATE_FORM_DATA' :

            return update(state , {
                formData : {
                    [action.key] : { $set : action.value }
                }
            });

        case 'SET_FORM_DATA':

            return update(state , {
                formData : {
                    $set : action.formData
                }
            });

        case 'CLEAR_FORM_DATA':

            return update(state , {
                formData : {
                    $set : {
                        title : '' ,
                        body : '' ,
                        imagePath : ''
                    }
                }
            });

        case 'PICK_IMAGE':

            return update(state , {
                formData : {
                    imagePath : {
                        $set : action.imagePath
                    }
                }
            });

        case 'REMOVE_PICKED_IMAGE' :

            return update(state , {
                formData : {
                    imagePath : {
                        $set : null
                    }
                }
            });

        case 'FORM_VALIDATION_FAILED':

            return update(state , {
                validationErrors : {
                    $set : {
                        title : action.title ,
                        body : action.body
                    }
                }
            });

        case 'CLEAR_VALIDATION_ERRORS':

            return update(state , {
                validationErrors : {
                    $set : {
                        title : '' ,
                        body : ''
                    }
                }
            });

        case 'GET_FEED_POSTS' :

            return update(state , {
                feedPosts : {
                    posts: {
                        $push : action.posts
                    } ,
                    page: {
                        $set : action.page
                    }
                }
            });

        case 'GET_SAVED_POSTS' :

            return update(state , {
                savedPosts : {
                    posts: {
                        $push : action.posts
                    } ,
                    page: {
                        $set : action.page
                    }
                }
            });

        case 'LOADING_FETCHING_POSTS' :

            return update(state , {
                loading : {
                    $set : action.status
                }
            });

        case 'LOADING_MORE_POSTS' :

            return update(state , {
                loadingMore : {
                    $set : action.status
                }
            });

        case 'ADD_NEW_POST' :

            return update(state , {
                feedPosts : {
                    posts : {
                        $unshift : [action.post]
                    }
                }
            });

        case 'UPDATE_POST' :

            let postIndex = state.feedPosts.posts.findIndex((post) => {
                return post.id == action.postId;
            });

            return update(state , {
                feedPosts : {
                    posts : {
                        [postIndex] : {
                            $set : action.post
                        }
                    }
                }
            });

        case 'DELETE_POST' :

            let index = state.feedPosts.posts.findIndex((post) => {
                return post.id == action.postId;
            });

            return update(state , {
                feedPosts : {
                    posts : {
                        $splice : [[index , 1]]
                    }
                }
            });

        case 'LIKE_POST' :

            return update(state , _.merge(
                updatePostInCollections(state , collections , action.postId , 'liked' , true),
                state.currentViewedPost ?
                    {
                        currentViewedPost : {
                            liked : {
                                $set : true
                            }
                        }
                    } :
                    {} ,
            ));

        case 'DISLIKE_POST' :

            return update(state , _.merge(
                updatePostInCollections(state , collections , action.postId , 'liked' , false) ,
                state.currentViewedPost ?
                    {
                        currentViewedPost : {
                            liked : {
                                $set : false
                            }
                        }
                    } :
                    {}
            ));

        case 'SAVE_POST' :

            return update(state , _.merge(
                updatePostInCollections(state , collections , action.postId , 'saved' , true) ,
                {
                    savedPosts : {
                        posts : {
                            $push : [
                                update(state[action.collection].posts.find((post) => post.id == action.postId) , {
                                    saved : {
                                        $set : true
                                    }
                                })
                            ]
                        }
                    }
                } ,
                state.currentViewedPost ?
                    {
                        currentViewedPost : {
                            saved : {
                                $set : true
                            }
                        }
                    } :
                    {}
            ));

        case 'UN_SAVE_POST' :

            return update(state , _.merge(
                updatePostInCollections(state , collections , action.postId , 'saved' , false) ,
                {
                    savedPosts : {
                        posts : {
                            $splice: [
                                [state.savedPosts.posts.findIndex((post) => post.id == action.postId) , 1]
                            ]
                        }
                    }
                } ,
                state.currentViewedPost ?
                    {
                        currentViewedPost : {
                            saved : {
                                $set : false
                            }
                        }
                    } :
                    {}
            ));

        case 'SET_CURRENT_VIEWED_POST' :

            return update(state , {
                currentViewedPost : {
                    $set : action.post
                }
            });

        case 'SET_PROFILE_POSTS' :

            return update(state , {
                profilePosts : {
                    posts : {
                        $set : action.posts
                    }
                }
            });

        default :
            return state;

    }

}