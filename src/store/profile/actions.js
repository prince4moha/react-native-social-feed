import { AsyncStorage } from 'react-native';
import axios from 'axios';
import _ from 'lodash';
import { setProfilePosts } from '../post/actions';
import { setAuthUser } from '../actions';

export const fetchUserProfile = (id) => {

    return (dispatch) => {

        dispatch(loading('LOADING_FETCHING_PROFILE_DATA' , true));

        axios({
            method : 'GET' ,
            url : 'profile'  ,
            params : {
                id
            }
        }).then(({ data }) => {

            dispatch({
                type : 'SET_PROFILE_DATA' ,
                user : data.data.user
            });

            dispatch(setProfilePosts(data.data.user.posts));

            dispatch(loading('LOADING_FETCHING_PROFILE_DATA' , false));

        }).catch((err) => {

            console.log(err);

            dispatch(loading(false));

        });

    };

};

export const follow = (id) => {

    return (dispatch) => {

        dispatch({ type : 'FOLLOW' });

        axios({
            method : 'GET' ,
            url: 'profile/follow' ,
            params : {
                user_id : id
            }
        }).then(({ data }) => {
        }).catch((err) => {
            console.log(err);
        });

    }

};

export const unFollow = (id) => {

    return (dispatch) => {

        dispatch({ type : 'UN_FOLLOW' });

        axios({
            method : 'GET' ,
            url: 'profile/un-follow' ,
            params : {
                user_id : id
            }
        }).then(({ data }) => {

        }).catch((err) => {
            console.log(err);
        });

    }

};

export const setProfileFormData = (formData) => {

    return {
        type : 'SET_PROFILE_FORM_DATA' ,
        formData
    }

};

export const updateProfileFormData = (key , value) => {

    return {
        type : 'UPDATE_PROFILE_FORM_DATA' ,
        key ,
        value
    }

};

export const updateProfile = () => {

    return (dispatch , getState) => {

        dispatch(loading('LOADING_UPDATING_PROFILE_FORM_DATA' , true));

        let formData = new FormData();

        let { name , email , profile_image } = getState().profile.profileFormData;

        formData.append('name' , name);
        formData.append('email' , email);
        formData.append('profile_image' , profile_image);

        axios({
            method : 'POST' ,
            url : 'profile/update' ,
            data : formData ,
        }).then(({ data }) => {

            console.log(data);

            // AsyncStorage.setItem('authUser' , JSON.stringify(data.data.user)).then(() => {
            //
            //     dispatch(setAuthUser(data.data.user));
            //
            // });
            //
            // console.log(data.data.user);
            //
            dispatch(loading('LOADING_UPDATING_PROFILE_FORM_DATA' , false));

        }).catch((err) => {

            console.log(err);
            dispatch(loading('LOADING_UPDATING_PROFILE_FORM_DATA' , false));

        });

    };

};

const loading = (type , status) => {

    return {
        type ,
        status
    }

};