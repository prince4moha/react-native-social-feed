import update from 'immutability-helper';

const initialState = {
    profileData : null ,
    loadingFetchingProfileData : false ,
    profileFormData : {
        name : '' ,
        email : '' ,
        profile_image : {
            uri : null ,
        }
    } ,
    loadingUpdatingProfileFormData : false
};

export default (state = initialState, action) => {

    switch (action.type) {

        case 'SET_PROFILE_DATA':

            return update(state , {
                profileData : {
                    $set: action.user
                }
            });

        case 'LOADING_FETCHING_PROFILE_DATA' :

            return update(state , {
                loadingFetchingProfileData : {
                    $set : action.status
                }
            });

        case 'FOLLOW':

            return update(state , {
                profileData : {
                    is_following : {
                        $set : true
                    } ,
                    followers_count : {
                        $set : state.profileData.followers_count + 1
                    }
                }
            });

        case 'UN_FOLLOW':

            return update(state , {
                profileData : {
                    is_following : {
                        $set : false
                    } ,
                    followers_count : {
                        $set : state.profileData.followers_count - 1
                    }
                }
            });

        case 'SET_PROFILE_FORM_DATA' :

            return update(state , {
                profileFormData : {
                    $set : action.formData
                }
            });

        case 'UPDATE_PROFILE_FORM_DATA' :

            return update(state , {
                profileFormData : {
                    [action.key] : {
                        $set : action.value
                    }
                }
            });

        case 'LOADING_UPDATING_PROFILE_FORM_DATA' :

            return update(state , {
                loadingUpdatingProfileFormData : {
                    $set : action.status
                }
            });

        default :

            return state;

    }

}