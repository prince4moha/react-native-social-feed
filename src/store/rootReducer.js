import {combineReducers} from 'redux';

import navReducer from './nav/navReducer';
import authReducer from './auth/authReducer';
import postReducer from './post/postReducer';
import commentReducer from './comment/commentReducer';
import profileReducer from './profile/profileReducer';

const rootReducer = combineReducers({
    nav : navReducer ,
    auth : authReducer ,
    post : postReducer ,
    comment : commentReducer ,
    profile : profileReducer
});

export default rootReducer;