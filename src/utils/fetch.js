import _ from 'lodash';

class fetcher {

    baseURL = '';

    params = {};

    get (config) {

        return new Promise((resolve , reject) => {

            fetch(this.baseURL + config.uri + this.getParams())
                .then(response => {response.json()})
                .then(responseJson => resolve(responseJson))
                .catch(err => reject(err));

        });

    }

    post (config) {

        return new Promise((resolve , reject) => {

            fetch(this.baseURL + config.uri + this.getParams() , {
                method : 'POST' ,
                headers : {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                } ,
                body : JSON.stringify(config.data)
            }).then(response => {response.json()})
                .then(responseJson => resolve(responseJson))
                .catch(err => reject(err));

        });

    }

    getParams() {

        var params = '';

        if (Object.keys(this.params).length > 0) {

            params = '?';

            for (let key in this.params) {

                params += key + '=' + this.params[key] + '&';

            }

            return _.trimEnd(params , '&');

        }

        return params;

    }

}

export default new fetcher();