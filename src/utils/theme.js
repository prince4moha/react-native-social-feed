export default {

    primaryColor : '#19B5FE' ,
    secondaryColor : '#4B77BE' ,
    primaryDanger : '#F44336' ,
    secondaryDanger : '#B71C1C'

}